import multer from "multer";
//import fs from 'file-system';

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    //console.log(file);
    if (
      (file.originalname !== undefined &&
        file.originalname !== null &&
        file.originalname.includes("miniBanner")) ||
      file.originalname.includes("showMiniBanner")
    )
      cb(null, "public/show_images/mini_banners");
    else if (
      (file.originalname !== undefined &&
        file.originalname !== null &&
        file.originalname.includes("responsiveBanner")) ||
      file.originalname.includes("showResponsiveBanner")
    )
      cb(null, "public/show_images/responsive_banners");
    else if (
      (file.originalname !== undefined &&
        file.originalname !== null &&
        file.originalname.includes("miniSiteBanner")) ||
      file.originalname.includes("showMiniSiteBanner")
    )
      cb(null, "public/show_images/mini_site_banners");
    else if (
      file.originalname !== undefined &&
      file.originalname !== null &&
      file.originalname.includes("responsiveMiniSiteBanner")
    )
      cb(null, "public/show_images/responsive_mini_site_banners");
    else if (
      file.originalname !== undefined &&
      file.originalname !== null &&
      file.originalname.includes("showBanner")
    )
      cb(null, "public/show_images/banners");
    else if (
      file.originalname !== undefined &&
      file.originalname !== null &&
      file.originalname.includes("advertiser")
    )
      cb(null, "public/show_images/advertisers");
    else cb(null, "public/show_images/");
  },
  filename: (req, file, cb) => {
    //console.log(file);
    var filetype = "";

    if (file.mimetype === "image/jpeg") {
      filetype = "jpg";
    }
    cb(null, file.originalname);
  },
});

var upload = multer({ storage: storage });

export default upload;
