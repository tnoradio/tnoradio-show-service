//BOOTSTRAP PATHS

import { ShowMysqlRepository } from "../core/infrastructure/show.mysql.repository";

import { ShowRepository } from "../core/domain/services/show.service.repository";

import { ShowImageRepository } from "../core/domain/services/show.images.repository";

import { CreateShowController } from "../core/application/controller/createShow.controller";
import { ShowCreator } from "../core/application/usecases/ShowCreator.uc";

import { RecomendedShowsController } from "../core/application/controller/getRecommendedShows.controller";

import { SelectedShowController } from "../core/application/controller/getShows.controller";
import { ShowSelect } from "../core/application/usecases/ShowSelect.uc";

import { EnabledShowsController } from "../core/application/controller/getEnabledShows.controller";
import { ShowsEnabled } from "../core/application/usecases/ShowsEnabled.uc";

import { GetByIdShowController } from "../core/application/controller/getShowById.controller";
import { ShowById } from "../core/application/usecases/ShowById.uc";

import { ShowByName } from "../core/application/usecases/ShowByName.uc";
import { GetByNameShowController } from "../core/application/controller/getShowByName.controller";

import { GetShowsAuthorizedByUserIdController } from "../core/application/controller/getShowsAuthorizedByUserId.controller";
import { ShowAuthorizedByUserId } from "../core/application/usecases/ShowAuthorizedByUserId.uc";

import { UpdateShowController } from "../core/application/controller/updateShowById.controller";
import { ShowUpdateById } from "../core/application/usecases/ShowUpdate.uc";

import { UpdateAdvertiserController } from "../core/application/controller/updateAdvertiserById.controller";
import { UpdateShowAdvertiser } from "../core/application/usecases/ShowAdvertiserUpdate.uc";

import { GetShowsOnWeekdayController } from "../core/application/controller/getShowsOnWeekday.controller";
import GetShowsOnWeekday from "../core/application/usecases/GetShowsOnWeekday.uc";

import { GetShowOnlineByTimeController } from "../core/application/controller/getOnlineShow.controller";
import { GetOnlineShowByTime } from "../core/application/usecases/GetOnlineShowByTime.uc";

import { GetShowBySlugController } from "../core/application/controller/getShowBySlug.controller";
import { GetShowBySlug } from "../core/application/usecases/GetShowBySlug.uc";

import { DestroyShowController } from "../core/application/controller/destroyShow.controller";
import { DestroyShow } from "../core/application/usecases/DestroyShow.uc";

import { DestroyShowAdvertiser } from "../core/application/usecases/DestroyShowAdvertiser.uc";
import { DestroyAdvertiserController } from "../core/application/controller/destroyAdvertiser.controller";

import { ShowGenres } from "../core/application/usecases/ShowGenres.uc";
import { ShowGenresController } from "../core/application/controller/getShowGenres.controller";

import { ShowPGs } from "../core/application/usecases/GetShowPGs.uc";
import { ShowPGsController } from "../core/application/controller/getShowsPGs.controller";

import { ShowTypes } from "../core/application/usecases/ShowTypes.uc";
import { ShowTypesController } from "../core/application/controller/getShowTypes.controller";

import { UploadImagesController } from "../core/application/controller/uploadImages.controller";
import { UploadImages } from "../core/application/usecases/UploadImages.uc";

import { HealthController } from "../core/application/controller/health.controller";
import { ShowsHealthGetter } from "../core/application/usecases/ShowsHealthGetter.uc";

import { GetShowImageController } from "../core/application/controller/getShowImage.controller";
import { GetShowImage } from "../core/application/usecases/GetShowImage.uc";

import { GetShowAdvertisersController } from "../core/application/controller/getShowAdvertisers.controller";
import { ShowAdvertisers } from "../core/application/usecases/ShowAdvertisers.uc";

import { ShowImagesLocalRepository } from "../core/infrastructure/show.images.local.repository";

import { GetShowWithHostsController } from "../core/application/controller/getShowWithHosts.controller";
import { ShowWithHosts } from "../core/application/usecases/ShowWithHosts.uc";

import { UploadImageToDBController } from "../core/application/controller/uploadImageToDBController";
import { UploadImageToDatabase } from "../core/application/usecases/UploadImageToBD.uc";

import { ShowImageDBGetter } from "../core/application/usecases/ShowImageDBGetter.uc";
import { GetImageFromDBController } from "../core/application/controller/getUserImageFromDBController";

import { UpdateImageInDB } from "../core/application/usecases/UpdateImageInDB.uc";
import { UpdateImageInDBController } from "../core/application/controller/updateImageInDBController";

import { ShowAdvertiserCreator } from "../core/application/usecases/uploadShowAdvertiser.uc";
import { CreateAdvertiserController } from "../core/application/controller/createShowAdController";

import { ShowBannersGetter } from "../core/application/usecases/GetShowBanners";
import { GetShowBannersController } from "../core/application/controller/getShowBannersController";

import { GoogleRepository } from "../core/infrastructure/ShowImages.google.repository";
import { UploadImage } from "../core/application/usecases/UploadImage.uc";
import { UploadImageController } from "../core/application/controller/uploadImage.controller";
import { RecomendedShows } from "../core/application/usecases/RecomendedShowsToBuyer.uc";

import { GetShowsByGenre } from "../core/application/usecases/GetShowsByGenre.uc";
import { GetShowsByGenreController } from "../core/application/controller/getShowsByGenre.controller";

import CreateShowSubgenreController from "../core/application/controller/createShowSubgenreController";
import CreateShowSubGenre from "../core/application/usecases/CreateShowSubGenre.uc";

import { ShowSubGenres } from "../core/application/usecases/GetShowSubgenres.uc";
import { ShowSubGenresController } from "../core/application/controller/getShowSubGenres.controller";

import { GetGenresWithShowsController } from "../core/application/controller/getGenresWithShows.controller";
import { GetGenresWithShows } from "../core/application/usecases/GetGenresWithShows.uc";
import { GetShowsByTodaysWeekday } from "../core/application/usecases/GetShowsByTodaysWeekday";
import { GetShowsByTodaysWeekdayController } from "../core/application/controller/getShowsByTodaysWeekday.controller";

import { GetPodcastsController } from "../core/application/controller/getPodcastsController";
import { GetPodcasts } from "../core/application/usecases/GetPodcasts";

import { CreateShowEpisodeController } from "../core/application/controller/createShowEpisode.controller";

import CreateShowEpisode from "../core/application/usecases/CreateShowEpisode";

import { GetShowSeasonsWithEpisodesController } from "../core/application/controller/getShowSeasonsWithEpisodesController";
import GetShowSeasonsWithEpisodes from "../core/application/usecases/GetShowSeasonsWithEpisodes";

import UpdateShowEpisodeController from "../core/application/controller/updateShowEpisode.controller";
import UpdateShowEpisode from "../core/application/usecases/UpdateShowEpisode";

import AddTagToEpisode from "../core/application/usecases/AddTagToEpisode.uc";

import GetEpisodeTagsController from "../core/application/controller/GetEpisodeTags.controller";
import GetEpisodeTags from "../core/application/usecases/GetEpisodeTags.uc";

import AddShowSeasonController from "../core/application/controller/addSeason.controller";
import AddShowSeason from "../core/application/usecases/AddShowSeason.uc";

import AddEpisodeTag from "../core/application/usecases/AddEpisodeTag.uc";
import { AddEpisodeTagController }  from "../core/application/controller/addEpisodeTag.controller";

import UpdateEpisodeTags from "../core/application/usecases/UpdateEpisodeTags.uc";
import UpdateEpisodeTagsController from "../core/application/controller/updateEpisodeTags.controller";
import { AddTagToEpisodeController } from "../core/application/controller/AddTagToEpisode.controller";

// BOOTSTRAP SERVICES
/**
 * Aquí se decide cuál implementación de las
 * diferentes infraestructuras se va a utilizar.
 */
const showRepository: ShowRepository = new ShowMysqlRepository();
const showImageRepository: ShowImageRepository =
  new ShowImagesLocalRepository();

const showGoogleRepository: ShowImageRepository = new GoogleRepository();

// BOOTSTRAP COMMANDS  (USE CASES)
const createShow = new ShowCreator({ showRepository });
const getShows = new ShowSelect({ showRepository });
const getEnabledShows = new ShowsEnabled({ showRepository });
const getShowById = new ShowById({ showRepository });
const showWithHosts = new ShowWithHosts({ showRepository });
const getShowByName = new ShowByName({ showRepository });
const getShowsAutorized = new ShowAuthorizedByUserId({ showRepository });
const updateShow = new ShowUpdateById({ showRepository });
const getShowsOnWeekday = new GetShowsOnWeekday(showRepository);
const getOnlineShowByTime = new GetOnlineShowByTime({ showRepository });
const getShowBySlug = new GetShowBySlug({ showRepository });
const destroyShow = new DestroyShow({ showRepository });
const getShowGenres = new ShowGenres({ showRepository });
const getShowPGs = new ShowPGs({ showRepository });
const getShowTypes = new ShowTypes({ showRepository });
const uploadImages = new UploadImages();
const showsHealthGetter = new ShowsHealthGetter();
const getShowImage = new GetShowImage({ showRepository });
const uploadImageToDb = new UploadImageToDatabase(showRepository);
const imageFromDbGetter = new ShowImageDBGetter(showRepository);
const updateImageInDB = new UpdateImageInDB(showRepository);
const createAdvertiser = new ShowAdvertiserCreator({ showRepository });
const showBannersGetter = new ShowBannersGetter({ showRepository });
const uploadImage = new UploadImage(showGoogleRepository);
const getShow = new ShowById({ showRepository });
const getShowAdvertisers = new ShowAdvertisers({ showRepository });
const advertiserUpdater = new UpdateShowAdvertiser({ showRepository });
const advertiserDestroyer = new DestroyShowAdvertiser({ showRepository });
const getRecomendedShows = new RecomendedShows({ showRepository });
const getShowsByGenre = new GetShowsByGenre({ showRepository });
const createShowSubGenre = new CreateShowSubGenre({ showRepository });
const getShowSubGenres = new ShowSubGenres({ showRepository });
const getGenresWithShows = new GetGenresWithShows({ showRepository });
const getShowsByTodaysWeekday = new GetShowsByTodaysWeekday({ showRepository });
const getPodcasts = new GetPodcasts({ showRepository });
const createShowEpisode = new CreateShowEpisode({ showRepository });
const getShowSeasonsWithEpisodes = new GetShowSeasonsWithEpisodes({
  showRepository,
});
const updateShowEpisode = new UpdateShowEpisode({ showRepository });
const addTagToEpisode = new AddTagToEpisode({ showRepository });
const getEpisodeTagsUseCase = new GetEpisodeTags({ showRepository });
const addShowSeason = new AddShowSeason({ showRepository });
const addEpisodeTag = new AddEpisodeTag({ showRepository });
const updateEpisodeTags = new UpdateEpisodeTags({ showRepository });

// BOOTSTRAP CONTROLLERS
const getRecomendedShowsController = new RecomendedShowsController({
  getRecomendedShows,
});
const showCreatorController = new CreateShowController({ createShow });
const showsSelectedController = new SelectedShowController({ getShows });
const showsEnabledController = new EnabledShowsController({ getEnabledShows });
const getShowByIdController = new GetByIdShowController({ getShowById });
const getShowByNameController = new GetByNameShowController({ getShowByName });
const getShowsAuthorizedByUserId = new GetShowsAuthorizedByUserIdController({
  getShowsAutorized,
});
const updateShowController = new UpdateShowController({ updateShow });
const getShowsOnWeekdayController = new GetShowsOnWeekdayController({
  getShowsOnWeekday,
});
const getShowOnlineByTimeController = new GetShowOnlineByTimeController({
  getOnlineShowByTime,
});
const getShowBySlugController = new GetShowBySlugController({ getShowBySlug });
const destroyShowController = new DestroyShowController({ destroyShow });
const getShowGenresController = new ShowGenresController({ getShowGenres });
const getShowPGsController = new ShowPGsController({ getShowPGs });
const getShowTypesController = new ShowTypesController({ getShowTypes });
const uploadImagesController = new UploadImagesController({ uploadImages });
const showsHealthController = new HealthController({ showsHealthGetter });
const getShowImageController = new GetShowImageController({ getShowImage });
const getShowsWithHostsController = new GetShowWithHostsController({
  showWithHosts,
});
const uploadImageToDbController = new UploadImageToDBController({
  uploadImageToDb,
});
const getImageFromDBController = new GetImageFromDBController({
  imageFromDbGetter,
});
const updateImageInDBController = new UpdateImageInDBController({
  updateImageInDB,
});
const createAdvertiserController = new CreateAdvertiserController({
  createAdvertiser,
});
const getShowBannersController = new GetShowBannersController({
  showBannersGetter,
});

const getShowAdvertisersController = new GetShowAdvertisersController({
  getShowAdvertisers,
});

const uploadImageController = new UploadImageController({
  uploadImage,
  updateShow,
  getShow,
  updateImageInDB,
});

const updateAdvertiserController = new UpdateAdvertiserController({
  advertiserUpdater,
});

const destroyAdvertiserController = new DestroyAdvertiserController({
  advertiserDestroyer,
});

const showsByGenreController = new GetShowsByGenreController({
  getShowsByGenre,
});

const createShowSubGenreController = new CreateShowSubgenreController({
  createShowSubGenre,
});

const getShowSubGenresController = new ShowSubGenresController({
  getShowSubGenres,
});

const getShowsByGenreController = new GetShowsByGenreController({
  getShowsByGenre,
});
const getGenresWithShowsController = new GetGenresWithShowsController({
  getGenresWithShows,
});

const getShowsByTodaysWeekdayController = new GetShowsByTodaysWeekdayController(
  {
    getShowsByTodaysWeekday,
  },
);

const getPodcastsController = new GetPodcastsController({ getPodcasts });

const createShowEpisodeController = new CreateShowEpisodeController({
  createShowEpisode,
});

const getShowSeasonsWithEpisodesController =
  new GetShowSeasonsWithEpisodesController({ getShowSeasonsWithEpisodes });
const updateShowEpisodeController = new UpdateShowEpisodeController({
  updateShowEpisode,
});

const addTagToEpisodeController = new AddTagToEpisodeController({
  addTagToEpisode,
});

const getEpisodeTagsController = new GetEpisodeTagsController({ getEpisodeTagsUseCase });

const addShowSeasonController = new AddShowSeasonController({ addShowSeason });

const addEpisodeTagController = new AddEpisodeTagController({ addEpisodeTag });

const updateEpisodeTagsController = new UpdateEpisodeTagsController({ updateEpisodeTags });

export {
  showCreatorController,
  showsSelectedController,
  getShowByIdController,
  getShowByNameController,
  getShowsAuthorizedByUserId,
  updateShowController,
  getShowsOnWeekdayController,
  showsEnabledController,
  getShowOnlineByTimeController,
  getShowBySlugController,
  destroyShowController,
  getShowGenresController,
  getShowPGsController,
  getShowTypesController,
  uploadImagesController,
  showsHealthController,
  getShowImageController,
  getShowsWithHostsController,
  uploadImageToDbController,
  updateImageInDBController,
  getImageFromDBController,
  createAdvertiserController,
  getShowBannersController,
  uploadImageController,
  getShowAdvertisersController,
  destroyAdvertiserController,
  updateAdvertiserController,
  getRecomendedShowsController,
  showsByGenreController,
  createShowSubGenreController,
  getShowSubGenresController,
  getShowsByGenreController,
  getGenresWithShowsController,
  getShowsByTodaysWeekdayController,
  getPodcastsController,
  createShowEpisodeController,
  getShowSeasonsWithEpisodesController,
  updateShowEpisodeController,
  addTagToEpisodeController,
  getEpisodeTagsController,
  addShowSeasonController,
  addEpisodeTagController,
  updateEpisodeTagsController
};
