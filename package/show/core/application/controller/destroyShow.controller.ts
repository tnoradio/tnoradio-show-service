import e from "express";
import { DestroyShow } from "../usecases/DestroyShow.uc";

export interface UsesCases {
  destroyShow: DestroyShow;
}

export class DestroyShowController {
  constructor(private usescases: UsesCases) {}

  async handle(req: e.Request, res: e.Response) {
    this.usescases.destroyShow.setIdShow(req.params.id);

    try {
      const show = await this.usescases.destroyShow.execute();

      return res.status(201).send(show);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
