import e from "express";
import AddTagToEpisode from "../usecases/AddTagToEpisode.uc";

interface UseCase {
  addTagToEpisode: AddTagToEpisode;
}

export class AddTagToEpisodeController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    this.useCase.addTagToEpisode.setEpisodeId(req.body.episodeId);
    this.useCase.addTagToEpisode.setTag(req.body.tag);

    try {
      const showEpisode = await this.useCase.addTagToEpisode.execute();
      return res.status(201).send(showEpisode);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}