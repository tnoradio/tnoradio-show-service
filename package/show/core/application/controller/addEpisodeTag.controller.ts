import e from "express";
import AddEpisodeTag from "../usecases/AddEpisodeTag.uc";

interface UseCase {
    addEpisodeTag: AddEpisodeTag;
}

export class AddEpisodeTagController {
    private useCase: UseCase;

    constructor(useCase: UseCase) {
        this.useCase = useCase;
    }

    public async handle(req: e.Request, res: e.Response) {
        try {
            console.log(req.body);
            this.useCase.addEpisodeTag.setTag(req.body);
            const tag = await this.useCase.addEpisodeTag.execute();
            res.status(201).json(tag);
        } catch (error) {
            res.status(400).json(error);
        }
    }
}