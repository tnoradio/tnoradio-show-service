import e from "express";
import { ShowAdvertisers } from "../usecases/ShowAdvertisers.uc";

export interface UseCase {
  getShowAdvertisers: ShowAdvertisers;
}

export class GetShowAdvertisersController {
  constructor(private useCases: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    this.useCases.getShowAdvertisers.setShowId(req.params.showId);

    try {
      const showAdvertisers = await this.useCases.getShowAdvertisers.execute();
      return res.status(200).send(showAdvertisers);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
