import e from "express";
import UpdateEpisodeTags from "../usecases/UpdateEpisodeTags.uc";

interface UseCase {
  updateEpisodeTags: UpdateEpisodeTags;
}

export default class UpdateEpisodeTagsController {
    private useCase: UseCase;
  
    constructor(useCase: UseCase) { 
        this.useCase = useCase;
    }

    async handle(req: e.Request, res: e.Response) {
          try {
    this.useCase.updateEpisodeTags.setEpisodeId(req.body.episodeId);
    this.useCase.updateEpisodeTags.setTags(req.body.tags);

      const updatedTags = await this.useCase.updateEpisodeTags.execute();
      return res.status(201).send(updatedTags);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}