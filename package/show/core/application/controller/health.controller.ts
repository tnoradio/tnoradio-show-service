import e from "express";
import { ShowsHealthGetter } from "../usecases/ShowsHealthGetter.uc";
import config from "../../../config";

export interface useCase {
  showsHealthGetter: ShowsHealthGetter;
}

/**
 * Always respond ok.
 */
export class HealthController {
  constructor(private useCase: useCase) {}

  async handle(req: e.Request, res: e.Response) {
    const response = `🛡️  Shows is ok on port: ${config.port} 🛡️`;

    try {
      return res.status(200).end(response);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
