import e from "express";
import { GetShowBySlug } from "../usecases/GetShowBySlug.uc";

export interface UseCase {
  getShowBySlug: GetShowBySlug;
}

export class GetShowBySlugController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    this.useCase.getShowBySlug.setShowSlug(req.params.slug);

    try {
      const show = await this.useCase.getShowBySlug.execute();
      return res.status(200).send(show);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
