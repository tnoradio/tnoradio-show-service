import e from "express";
import { ShowAuthorizedByUserId } from "../usecases/ShowAuthorizedByUserId.uc";

export interface UsesCases {
  getShowsAutorized: ShowAuthorizedByUserId;
}

export class GetShowsAuthorizedByUserIdController {
  constructor(private usescases: UsesCases) {}

  async handle(req: e.Request, res: e.Response) {
    this.usescases.getShowsAutorized.setIdUser(req.params.id);

    try {
      const shows = await this.usescases.getShowsAutorized.execute();

      return res.status(200).send(shows);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
