import e from "express";
import { UpdateShowAdvertiser } from "../usecases/ShowAdvertiserUpdate.uc";

export interface UseCase {
  advertiserUpdater: UpdateShowAdvertiser;
}

export class UpdateAdvertiserController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    this.useCase.advertiserUpdater.setUpdateValues(req.body);

    try {
      const response = await this.useCase.advertiserUpdater.execute();
      return res.status(200).send(response);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
