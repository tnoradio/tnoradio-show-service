import e from "express";
import { UploadImageToDatabase } from "../usecases/UploadImageToBD.uc";
var fs = require("fs");
var path = require("path");

interface UseCase {
  uploadImageToDb: UploadImageToDatabase;
}

export class UploadImageToDBController {
  constructor(private useCase: UseCase) {}

  async handle(req, res) {
    var obj = {
      id: null,
      name: req.body.name,
      showSlug: req.body.slug,
      show: req.body.showId,
      file: fs.readFileSync(
        path.join(
          "public/show_images/" + req.body.location + "/",
          req.file.filename,
        ),
      ),
    };
    this.useCase.uploadImageToDb.SetImage(obj);
    try {
      const image = await this.useCase.uploadImageToDb.execute();
      return res.status(201).send(image);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
