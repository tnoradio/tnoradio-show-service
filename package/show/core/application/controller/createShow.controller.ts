import e from "express";
import { ShowCreator } from "../usecases/ShowCreator.uc";

export interface UseCase {
  createShow: ShowCreator;
}

export class CreateShowController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    this.useCase.createShow.setShow(req.body);

    console.log(this.useCase.createShow.getShow());

    try {
      const show = await this.useCase.createShow.execute();
      console.log(show);
      return res.status(201).send(show);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
