import e from "express";
import CreateShowSubGenre from "../usecases/CreateShowSubGenre.uc";

interface UseCase {
  createShowSubGenre: CreateShowSubGenre;
}

export default class CreateShowSubgenreController {
  constructor(private useCase: UseCase) {}

  public async handle(req: e.Request, res: e.Response) {
    try {
      this.useCase.createShowSubGenre.setGenreId(+req.query.genreId);
      this.useCase.createShowSubGenre.setSubGenre(
        req.query.subgenre.toString(),
      );
      const response = await this.useCase.createShowSubGenre.execute();
      res.status(200).json(response);
    } catch (error) {
      res.status(500).json(error);
    }
  }
}
