import e from "express";
import { GetShowImage } from "../usecases/GetShowImage.uc";

export interface UseCase {
  getShowImage: GetShowImage;
}

export class GetShowImageController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    try {
      this.useCase.getShowImage.setShowSlug(req.params.showslug);
      this.useCase.getShowImage.setImageName(req.params.imagename);
      const response = await this.useCase.getShowImage.execute();
      return res.status(200).send(response);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
