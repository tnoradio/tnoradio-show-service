import e from "express";
import GetShowSeasonsWithEpisodes from "../usecases/GetShowSeasonsWithEpisodes";

export interface UseCase {
  getShowSeasonsWithEpisodes: GetShowSeasonsWithEpisodes;
}

export class GetShowSeasonsWithEpisodesController {
  constructor(private useCase: UseCase) {}

  public getShowSeasonsWithEpisodes(req: e.Request, res: e.Response) {
    const showId = JSON.parse(req.params.showId);
    this.useCase.getShowSeasonsWithEpisodes.setShowId(showId);
    this.useCase.getShowSeasonsWithEpisodes
      .execute()
      .then((result) => {
        res.status(200).json(result);
      })
      .catch((error) => {
        res.status(500).json(error);
      });
  }
}
