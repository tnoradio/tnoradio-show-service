import e from "express";
import { ShowGenres } from "../usecases/ShowGenres.uc";

export interface useCase {
  getShowGenres: ShowGenres;
}

export class ShowGenresController {
  constructor(private useCase: useCase) {}

  async handle(req: e.Request, res: e.Response) {
    try {
      const showGenres = await this.useCase.getShowGenres.execute();

      return res.status(200).send(showGenres);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
