import e from "express";
import { ShowById } from "../usecases/ShowById.uc";

export interface UseCase {
  getShowById: ShowById;
}

export class GetByIdShowController {
  constructor(private useCases: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    this.useCases.getShowById.setIdShow(req.params.id);
    try {
      const show = await this.useCases.getShowById.execute();
      return res.status(200).send(show);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
