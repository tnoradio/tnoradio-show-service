import e from "express";
import { GetOnlineShowByTime } from "../usecases/GetOnlineShowByTime.uc";

export interface UseCase {
  getOnlineShowByTime: GetOnlineShowByTime;
}

export class GetShowOnlineByTimeController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    console.log("Controller " + req.params.time);

    this.useCase.getOnlineShowByTime.setTime(req.params.time);

    try {
      const show = await this.useCase.getOnlineShowByTime.execute();
      console.log(show);
      return res.status(200).send(show);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
