import e from "express";
import UpdateShowEpisode from "../usecases/UpdateShowEpisode";

interface UseCase {
  updateShowEpisode: UpdateShowEpisode;
}

export default class UpdateShowEpisodeController {
  constructor(private useCase: UseCase) {}

  public async updateShowEpisode(req: e.Request, res: e.Response) {
    try {
      const episode = req.body;
      this.useCase.updateShowEpisode.setEpisode(episode);
      const response = await this.useCase.updateShowEpisode.execute();
      res.status(200).json(response);
    } catch (error) {
      res.status(400).json(error);
    }
  }
}
