import e from "express";
import { UpdateImageInDB } from "../usecases/UpdateImageInDB.uc";
var fs = require("fs");
var path = require("path");

interface UseCase {
  updateImageInDB: UpdateImageInDB;
}

export class UpdateImageInDBController {
  constructor(private useCase: UseCase) {}

  async handle(req, res) {
    var obj = {
      id: req.body.imageId,
      name: req.body.name,
      showSlug: req.body.slug,
      file: fs.readFileSync(
        path.join(
          "public/show_images/" + req.body.location + "/",
          req.file.filename,
        ),
      ),
      url: "",
    };

    this.useCase.updateImageInDB.SetImage(obj);
    this.useCase.updateImageInDB.SetName(req.body.type);
    this.useCase.updateImageInDB.SetSlug(req.body.slug);
    this.useCase.updateImageInDB.SetUrl(req.body.url || "");

    try {
      const image = await this.useCase.updateImageInDB.execute();
      return res.status(200).send(image);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
