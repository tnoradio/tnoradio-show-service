import e from "express";
import { ShowSelect } from "../usecases/ShowSelect.uc";

export interface useCase {
  getShows: ShowSelect;
}

export class SelectedShowController {
  constructor(private useCase: useCase) {}

  async handle(req: e.Request, res: e.Response) {
    try {
      const shows = await this.useCase.getShows.execute();

      return res.status(200).send(shows);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
