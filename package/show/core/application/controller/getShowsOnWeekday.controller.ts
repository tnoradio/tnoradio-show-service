import e from "express";
import GetShowsOnWeekday from "../usecases/GetShowsOnWeekday.uc";

export interface UseCase {
  getShowsOnWeekday: GetShowsOnWeekday;
}

export class GetShowsOnWeekdayController {
  useCase: UseCase;

  constructor(useCase: UseCase) {
    this.useCase = useCase;
  }

  async handle(req: e.Request, res: e.Response) {
    //console.log(req.params.weekday);
    this.useCase.getShowsOnWeekday.setWeekday(req.params.weekday);

    try {
      const shows = await this.useCase.getShowsOnWeekday.run();

      //console.log(shows);

      return res.status(200).send(shows);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
