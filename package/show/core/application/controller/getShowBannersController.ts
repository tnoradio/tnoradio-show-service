import e from "express";
import { ShowBannersGetter } from "../usecases/GetShowBanners";

export interface UseCase {
  showBannersGetter: ShowBannersGetter;
}

export class GetShowBannersController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    try {
      this.useCase.showBannersGetter.setImageName(req.params.imageName);

      const banners = await this.useCase.showBannersGetter.execute();
      //console.log(banners.length);
      return res.status(200).send(banners);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
