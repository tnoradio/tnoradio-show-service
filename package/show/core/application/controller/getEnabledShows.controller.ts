import e from "express";
import { ShowsEnabled } from "../usecases/ShowsEnabled.uc";

export interface UseCase {
  getEnabledShows: ShowsEnabled;
}

export class EnabledShowsController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    try {
      const shows = await this.useCase.getEnabledShows.execute();

      return res.status(200).send(shows);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
