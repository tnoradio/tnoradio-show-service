import e from "express";
import GetEpisodeTags from "../usecases/GetEpisodeTags.uc";

interface UseCase {
    getEpisodeTagsUseCase: GetEpisodeTags;
}

export default class GetEpisodeTagsController {
  constructor(private useCase: UseCase) {}

  async execute(req: e.Request, res: e.Response) {
    try {
      const result = await this.useCase.getEpisodeTagsUseCase.execute();
      return res.status(200).json(result);
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  }
}