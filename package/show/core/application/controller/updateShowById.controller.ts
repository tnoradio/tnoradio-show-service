import e from "express";
import { ShowUpdateById } from "../usecases/ShowUpdate.uc";

export interface UseCase {
  updateShow: ShowUpdateById;
}

export class UpdateShowController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    this.useCase.updateShow.setUpdateValues(req.body);

    try {
      const show = await this.useCase.updateShow.execute();
      return res.status(200).send(show);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
