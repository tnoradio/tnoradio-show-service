import e from "express";
import { RecomendedShows } from "../usecases/RecomendedShowsToBuyer.uc";

export interface UseCase {
  getRecomendedShows: RecomendedShows;
}

export class RecomendedShowsController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    try {
      this.useCase.getRecomendedShows.setGenres(req.query.genres);
      this.useCase.getRecomendedShows.setGenders(req.query.genders);
      this.useCase.getRecomendedShows.setTarget(req.query.target);
      this.useCase.getRecomendedShows.setAgeRanges(req.query.age_ranges);
      const shows = await this.useCase.getRecomendedShows.execute();
      const response = shows.slice(0, 5);
      return res.status(200).send(response);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
