import e from "express";
import CreateShowEpisode from "../usecases/CreateShowEpisode";

export interface UseCase {
  createShowEpisode: CreateShowEpisode;
}

export class CreateShowEpisodeController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    this.useCase.createShowEpisode.setEpisodeData(req.body);

    try {
      const show = await this.useCase.createShowEpisode.execute();
      console.log(show);
      return res.status(201).send(show);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
