import e from "express";
import { GetShowsByTodaysWeekday } from "../usecases/GetShowsByTodaysWeekday";

export interface UseCase {
  getShowsByTodaysWeekday: GetShowsByTodaysWeekday;
}

export class GetShowsByTodaysWeekdayController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    try {
      const shows = await this.useCase.getShowsByTodaysWeekday.execute();
      return res.status(200).send(shows);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
