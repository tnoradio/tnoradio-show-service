import e from "express";
import AddShowSeason  from "../usecases/AddShowSeason.uc";

interface UseCase {
    addShowSeason: AddShowSeason;
}

export default class AddSeasonController {
    private useCase: UseCase;

    constructor(useCase: UseCase) {
        this.useCase = useCase;
    }

    async handle(req: e.Request, res: e.Response) {
        try {
            this.useCase.addShowSeason.setShowId(req.body.showId);
            this.useCase.addShowSeason.setSeason(req.body.season_number);
            const season = await this.useCase.addShowSeason.execute();
            return res.status(201).send({id: season.id, season_number: season.season_number, showId: season.show.id, episodes: season.episodes});
        } catch (error) {
            console.log(error);
            return res.status(400).send(error);
        }
    }
}