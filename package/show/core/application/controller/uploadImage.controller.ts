import e from "express";
import { Show } from "../../domain/entities/Show";
import { ShowImage } from "../../domain/entities/showImage";
import { UploadImage } from "../usecases/UploadImage.uc";
import { UpdateImageInDB } from "../usecases/UpdateImageInDB.uc";
import { ShowUpdateById } from "../usecases/ShowUpdate.uc";
import { ShowById } from "../usecases/ShowById.uc";

var fs = require("fs");
var path = require("path");

interface UseCase {
  uploadImage: UploadImage;
  updateShow: ShowUpdateById;
  getShow: ShowById;
  updateImageInDB: UpdateImageInDB;
}
let show: Show;
let showImage: ShowImage;

export class UploadImageController {
  constructor(private useCase: UseCase) {}

  async handle(req: any, res: e.Response) {
    try {
      this.useCase.getShow.setIdShow(req.body.showId);
      show = await this.useCase.getShow.execute();
      this.useCase.uploadImage.setMimeType("image/*");
      this.useCase.uploadImage.setName(req.file.filename);
      this.useCase.uploadImage.setPath(
        "public/show_images/" + req.body.location + "/" + req.file.filename,
      );

      const imageUrl = await this.useCase.uploadImage.execute();
      showImage = new ShowImage(
        req.body.showId,
        req.body.name,
        req.body.slug,
        null,
        imageUrl,
      );

      if (show) {
        this.useCase.updateImageInDB.SetImage(showImage);
        this.useCase.updateImageInDB.SetName(req.body.name);
        this.useCase.updateImageInDB.SetSlug(req.body.slug);
        this.useCase.updateImageInDB.SetUrl(imageUrl);
      }

      const response = await this.useCase.updateImageInDB.execute();

      console.log(response);

      fs.unlink(
        path.join(
          "public/show_images/" + req.body.location + "/" + req.file.filename,
        ),
        (err) => {
          if (err) {
            console.error(err);
            return res.status(400).send(err);
          }
        },
      );
      return res.status(200).send(showImage);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
