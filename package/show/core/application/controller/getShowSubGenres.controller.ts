import e from "express";
import { ShowSubGenres } from "../usecases/GetShowSubgenres.uc";

export interface useCase {
  getShowSubGenres: ShowSubGenres;
}

export class ShowSubGenresController {
  constructor(private useCase: useCase) {}

  async handle(req: e.Request, res: e.Response) {
    try {
      const showSubGenres = await this.useCase.getShowSubGenres.execute();
      return res.status(200).send(showSubGenres);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
