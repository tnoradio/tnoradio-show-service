import e from "express";
import { ShowPGs } from "../usecases/GetShowPGs.uc";
export interface useCase {
  getShowPGs: ShowPGs;
}

export class ShowPGsController {
  constructor(private useCase: useCase) {}

  async handle(req: e.Request, res: e.Response) {
    try {
      const showPGs = await this.useCase.getShowPGs.execute();

      return res.status(200).send(showPGs);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
