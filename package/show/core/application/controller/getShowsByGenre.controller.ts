import e from "express";
import { GetShowsByGenre } from "../usecases/GetShowsByGenre.uc";

export interface UseCase {
  getShowsByGenre: GetShowsByGenre;
}

export class GetShowsByGenreController {
  useCase: UseCase;

  constructor(useCase: UseCase) {
    this.useCase = useCase;
  }

  async handle(req: e.Request, res: e.Response) {
    this.useCase.getShowsByGenre.setGenre(req.params.genre);

    try {
      const shows = await this.useCase.getShowsByGenre.execute();
      return res.status(200).send(shows);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
