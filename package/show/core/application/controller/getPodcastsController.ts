import { Request, Response } from "express";
import { GetPodcasts } from "../usecases/GetPodcasts";

export interface UseCase {
  getPodcasts: GetPodcasts;
}

export class GetPodcastsController {
  constructor(private useCase: UseCase) {}

  async execute(req: Request, res: Response) {
    try {
      const shows = await this.useCase.getPodcasts.execute();
      return res.status(200).send(shows);
    } catch (error) {
      return res.status(400).send(error);
    }
  }
}
