import e from "express";
import { ShowByName } from "../usecases/ShowByName.uc";

export interface UsesCases {
  getShowByName: ShowByName;
}

export class GetByNameShowController {
  constructor(private usescases: UsesCases) {}

  async handle(req: e.Request, res: e.Response) {
    this.usescases.getShowByName.setNameShow(req.params.showName);

    try {
      const shows = await this.usescases.getShowByName.execute();

      return res.status(201).send(shows);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
