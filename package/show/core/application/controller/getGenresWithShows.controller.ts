import e from "express";
import { GetGenresWithShows } from "../usecases/GetGenresWithShows.uc";

export interface useCase {
  getGenresWithShows: GetGenresWithShows;
}

export class GetGenresWithShowsController {
  constructor(private useCase: useCase) {}

  async handle(req: e.Request, res: e.Response) {
    try {
      const genresWithShows = await this.useCase.getGenresWithShows.execute();
      return res.status(200).send(genresWithShows);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
