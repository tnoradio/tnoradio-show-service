import e from "express";
import { ShowTypes } from "../usecases/ShowTypes.uc";

export interface useCase {
  getShowTypes: ShowTypes;
}

export class ShowTypesController {
  constructor(private useCase: useCase) {}

  async handle(req: e.Request, res: e.Response) {
    try {
      const showTypes = await this.useCase.getShowTypes.execute();

      return res.status(200).send(showTypes);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
