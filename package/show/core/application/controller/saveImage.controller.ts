import e from "express";
import { SaveImage } from "../usecases/SaveShowImage.uc";

export interface UseCase {
  saveImage: SaveImage;
}

export class SaveImageController {
  constructor(private useCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    //this.useCase.createShow.setShow(req.body);

    try {
      const imageURL = ""; //await this.useCase.createShow.execute();

      return res.status(201).send(imageURL);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
