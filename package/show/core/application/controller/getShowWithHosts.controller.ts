import e from "express";
//import kafka from "../../../loaders/kafka";
import { ShowWithHosts } from "../usecases/ShowWithHosts.uc";

export interface UseCase {
  showWithHosts: ShowWithHosts;
}

export class GetShowWithHostsController {
  constructor(private useCases: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    this.useCases.showWithHosts.setShowSlug(req.params.slug);

    try {
      const show = await this.useCases.showWithHosts.execute();
      var hostsRes = [];
      var showWithHosts = {};

      /* if (
        show !== undefined &&
        show.hosts !== undefined &&
        show.hosts.length !== 0
      ) {
        show.hosts.forEach((showHost) => {
          kafka.producer.send(
            [
              {
                topic: "shows",
                messages: showHost.userId,
                key: showHost.userId,
              },
            ],
            function (err, data) {
              kafka.consumer.on("message", function (message) {
                if (message.key === showHost.userId) {
                  hostsRes.push(JSON.parse(message.value.toString()));

                  if (hostsRes.length === show.hosts.length) {
                    showWithHosts = {
                      show: show,
                      hostsData: hostsRes,
                    };

                    return res.status(200).send(showWithHosts);
                  }
                }
              });

              kafka.consumer.on("error", (err) => {
                console.log("Error en kafka consumer");
                console.log(err);
              });

              if (err) {
                console.log("Error en message send");
                console.log(err);
                return res
                  .status(400)
                  .send("Error sending the message with kafka");
              }
            }
          );
        });
      } else {
        return res.status(400).send("Show with no hosts");
      }*/
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
