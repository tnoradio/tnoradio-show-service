import e from "express";
import { DestroyShowAdvertiser } from "../usecases/DestroyShowAdvertiser.uc";

export interface UseCase {
  advertiserDestroyer: DestroyShowAdvertiser;
}

export class DestroyAdvertiserController {
  constructor(private UseCase: UseCase) {}

  async handle(req: e.Request, res: e.Response) {
    this.UseCase.advertiserDestroyer.setIdAdvertiser(req.params.id);

    try {
      const response = await this.UseCase.advertiserDestroyer.execute();

      return res.status(200).send(response);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
