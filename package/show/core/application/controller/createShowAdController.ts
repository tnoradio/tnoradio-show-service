import e from "express";
import { ShowAdvertiserCreator } from "../usecases/uploadShowAdvertiser.uc";
var fs = require("fs");
var path = require("path");

interface UseCase {
  createAdvertiser: ShowAdvertiserCreator;
}

export class CreateAdvertiserController {
  constructor(private useCase: UseCase) {}

  async handle(req: any, res: e.Response) {
    var obj = {
      id: null,
      name: req.body.name,
      website: req.body.website,
      show: req.body.showId,
      image: fs.readFileSync(
        path.join(
          "public/show_images/" + "advertisers" + "/",
          req.file.filename,
        ),
      ),
    };

    this.useCase.createAdvertiser.setAdvertiser(obj);

    try {
      const advertiser = await this.useCase.createAdvertiser.execute();
      return res.status(201).send(advertiser);
    } catch (error) {
      console.log(error);
      return res.status(400).send(error);
    }
  }
}
