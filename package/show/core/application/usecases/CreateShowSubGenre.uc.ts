import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export default class CreateShowSubGenre extends Command {
  private subGenre: string;
  genreId: number;

  constructor(private services: Services) {
    super();
  }

  public getSubGenre(): string {
    return this.subGenre;
  }

  public setSubGenre(subGenre: string) {
    this.subGenre = subGenre;
  }

  public getGenreId(): number {
    return this.genreId;
  }

  public setGenreId(genreId: number) {
    this.genreId = genreId;
  }

  async execute() {
    try {
      const res = await this.services.showRepository.createShowSubGenre(
        this.getGenreId(),
        this.getSubGenre(),
      );
      return res;
    } catch (error) {
      return error;
    }
  }
}
