import { ShowEpisodeTag } from "../../domain/entities/ShowEpisodeTag";
import { ShowRepository } from "../../domain/services/show.service.repository";
import command from "../command";

interface services {
    showRepository: ShowRepository;
}

export default class AddTagToEpisode extends command {
    private episodeId: number;
    private tag: ShowEpisodeTag;

    constructor(private services: services) {
        super();
    }

    public getTag() {
        return this.tag;
    }

    public getEpisodeId() {
        return this.episodeId;
    }

    public setEpisodeId(showId: number) {
        this.episodeId = showId;
    }   

    public setTag(tag: ShowEpisodeTag) {
        this.tag = tag;
    }

    async execute() {
        try {
            const res = await this.services.showRepository.addTagToEpisode(this.getTag(), this.getEpisodeId());
            return res;
        } catch (error) {
            return error;
        }
    }
}