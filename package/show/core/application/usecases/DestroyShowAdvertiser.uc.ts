import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class DestroyShowAdvertiser extends Command {
  public _idAdvertiser;

  constructor(private services: Services) {
    super();
  }

  public setIdAdvertiser(id) {
    this._idAdvertiser = id;
  }

  public getIdAdvertiser() {
    return this._idAdvertiser;
  }

  public async execute() {
    const response = await this.services.showRepository.destroyAdvertiser(
      this._idAdvertiser,
    );
    return response;
  }
}
