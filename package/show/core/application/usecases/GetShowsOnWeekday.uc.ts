import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export default class GetShowsOnWeekday {
  public weekday: string;
  showRepository: ShowRepository;

  constructor(showRepository: ShowRepository) {
    this.showRepository = showRepository;
  }

  public setWeekday(weekday) {
    this.weekday = weekday;
  }

  public getWeekday() {
    return this.weekday;
  }

  public async run() {
    const response = await this.showRepository.getShowsOnWeekday(this.weekday);
    return response;
  }
}
