import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export default class GetShowSeasonsWithEpisodes extends Command {
  private showId: number;

  constructor(private services: Services) {
    super();
  }

  public getShowId(): number {
    return this.showId;
  }

  public setShowId(showId: number) {
    this.showId = showId;
  }

  async execute() {
    try {
      const res =
        await this.services.showRepository.fetchShowSeasonsWithEpisodes(
          this.getShowId(),
        );
      return res;
    } catch (error) {
      return error;
    }
  }
}
