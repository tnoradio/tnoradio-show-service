import Command from "../command";

import { ShowImageRepository } from "../../domain/services/show.images.repository";

export interface Services {
  showImageRepository: ShowImageRepository;
}

export class GetShowMiniBanner extends Command {
  public showImageUrl: string;

  constructor(private services: Services) {
    super();
  }

  public setShowImageUrl(showImageUrl) {
    this.showImageUrl = showImageUrl;
  }

  public getShowImageUrl() {
    return this.showImageUrl;
  }

  //  Override Method
  public async execute() {
    const response = await this.services.showImageRepository.getShowImage(
      this.showImageUrl,
    );
    return response;
  }
}
