import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class GetShowsByGenre extends Command {
  private genre: string;

  constructor(private services: Services) {
    super();
  }

  public getGenre() {
    return this.genre;
  }

  public setGenre(genre: string) {
    this.genre = genre;
  }

  public async execute() {
    const response = await this.services.showRepository.getShowsByGenre(
      this.getGenre(),
    );
    return response;
  }
}
