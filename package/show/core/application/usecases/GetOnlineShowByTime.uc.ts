import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class GetOnlineShowByTime extends Command {
  public time: Date;

  constructor(private services: Services) {
    super();
  }

  public setTime(time) {
    this.time = time;
  }

  public getTime() {
    return this.time;
  }

  public async execute() {
    const response = await this.services.showRepository.getOnlineShowByTime(
      this.time,
    );
    return response;
  }
}
