import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class ShowById extends Command {
  private _idShow: number;

  constructor(private services: Services) {
    super();
  }

  public setIdShow(id) {
    this._idShow = id;
  }

  public getIdShow() {
    return this._idShow;
  }

  public async execute() {
    const response = await this.services.showRepository.getShowById(
      this._idShow,
    );
    return response;
  }
}
