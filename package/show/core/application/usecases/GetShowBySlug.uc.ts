import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class GetShowBySlug extends Command {
  public showSlug: string;

  constructor(private services: Services) {
    super();
  }

  public setShowSlug(slug) {
    this.showSlug = slug;
  }

  public getShowSlug() {
    return this.showSlug;
  }

  public async execute() {
    const response = await this.services.showRepository.getShowBySlug(
      this.showSlug,
    );
    return response;
  }
}
