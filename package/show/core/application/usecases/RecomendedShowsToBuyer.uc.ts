import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class RecomendedShows extends Command {
  private genders: string[];
  private target: string[];
  private genres: string[];
  private age_ranges: string[];

  constructor(private services: Services) {
    super();
  }

  public setGenders(genders) {
    this.genders = genders;
  }
  public setGenres(genres) {
    this.genres = genres;
  }
  public setTarget(target) {
    this.target = target;
  }
  public setAgeRanges(age_ranges) {
    this.age_ranges = age_ranges;
  }

  public async execute() {
    const response = await this.services.showRepository.getMatchingShows(
      this.genders,
      this.genres,
      this.target,
      this.age_ranges,
    );
    return response;
  }
}
