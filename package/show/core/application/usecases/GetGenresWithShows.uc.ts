import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class GetGenresWithShows extends Command {
  constructor(private services: Services) {
    super();
  }

  public async execute() {
    const response = await this.services.showRepository.getGenresWithShows();
    return response;
  }
}
