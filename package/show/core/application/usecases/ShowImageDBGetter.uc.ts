import { ShowRepository } from "../../domain/services/show.service.repository";
import Command from "../command";

/**
 * Gets user image from database.
 */
export class ShowImageDBGetter extends Command {
  private _repository: ShowRepository;
  private name: String;
  private slug: String;

  constructor(repository: ShowRepository) {
    super();
    this._repository = repository;
  }

  public setImageName(name: String) {
    this.name = name;
  }

  public setImageSlug(slug: String) {
    this.slug = slug;
  }

  public getName() {
    return this.name;
  }

  public getSlug() {
    return this.slug;
  }
  //  Override Method
  async execute() {
    const response = await this._repository.getShowImage(
      this.getName(),
      this.getSlug(),
    );
    return response;
  }
}
