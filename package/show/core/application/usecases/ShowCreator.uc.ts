import { Show } from "../../domain/entities/Show";
import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class ShowCreator extends Command {
  private _show: Show;

  constructor(private services: Services) {
    super();
  }

  public setShow(show: Show) {
    this._show = show;
  }

  public getShow() {
    return this._show;
  }

  public async execute() {
    const response = await this.services.showRepository.createShow(this._show);
    return response;
  }
}
