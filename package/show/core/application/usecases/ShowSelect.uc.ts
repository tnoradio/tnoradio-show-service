import { Show } from "../../domain/entities/Show";
import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class ShowSelect extends Command {
  constructor(private services: Services) {
    super();
  }

  //  Override Method
  public async execute() {
    const response = await this.services.showRepository.getAllShows();
    return response;
  }
}
