import { Show } from "../../domain/entities/Show";
import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class ShowWithHosts extends Command {
  public showSlug: string;

  constructor(private services: Services) {
    super();
  }

  public setShowSlug(slug) {
    this.showSlug = slug;
  }

  public getIdShow() {
    return this.showSlug;
  }

  //  Override Method
  public async execute() {
    const response = await this.services.showRepository.getShowBySlug(
      this.showSlug,
    );
    return response;
  }
}
