//import { Show } from '../../domain/entities/Show';
import Command from "../command";
import { ShowImageRepository } from "../../domain/services/show.images.repository";

export class UploadImage extends Command {
  private _repository: ShowImageRepository;
  private name: String;
  private mimeType: String;
  private path: String;

  constructor(_repository: ShowImageRepository) {
    super();
    this._repository = _repository;
  }

  public setName(name) {
    this.name = name;
  }

  public getName() {
    return this.name;
  }
  public setMimeType(mimeType: String) {
    this.mimeType = mimeType;
  }

  public getMimeTyoe() {
    return this.mimeType;
  }
  public setPath(path) {
    this.path = path;
  }

  public getPath() {
    return this.path;
  }

  //  Override Method
  public async execute() {
    const response = await this._repository.uploadFile(
      this.getName(),
      this.getMimeTyoe(),
      this.getPath(),
    );
    return response;
  }
}
