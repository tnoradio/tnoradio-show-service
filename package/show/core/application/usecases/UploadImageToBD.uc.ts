import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";
import { ShowImage } from "../../domain/entities/showImage";

/**
 * Uploads image to database.
 */
export class UploadImageToDatabase extends Command {
  private _repository: ShowRepository;
  private image: ShowImage;

  constructor(_repository: ShowRepository) {
    super();
    this._repository = _repository;
  }

  public SetImage(image: ShowImage) {
    this.image = image;
  }

  public getImage() {
    return this.image;
  }

  //  Override Method
  public async execute() {
    const response = await this._repository.saveImage(this.image);
    return response;
  }
}
