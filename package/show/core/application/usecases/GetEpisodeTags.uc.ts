import command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

interface services {
    showRepository: ShowRepository;
}

export default class GetEpisodeTags extends command {

    constructor(private services: services) {
        super();
    }


    async execute() {
        try {
            const res = await this.services.showRepository.getEpisodeTags();
            return res;
        } catch (error) {
            return error;
        }
    }
}