import { Show } from "../../domain/entities/Show";
import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";
import { ShowAdvertiser } from "../../domain/entities/ShowAdvertiser";

export interface Services {
  showRepository: ShowRepository;
}

export class ShowAdvertiserCreator extends Command {
  private showAd: ShowAdvertiser;

  constructor(private services: Services) {
    super();
  }

  public setAdvertiser(show: ShowAdvertiser) {
    this.showAd = show;
  }

  public getAdvertiser() {
    return this.showAd;
  }

  public async execute() {
    const response = await this.services.showRepository.saveAdvertiser(
      this.showAd,
    );
    return response;
  }
}
