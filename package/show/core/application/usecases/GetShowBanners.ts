import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class ShowBannersGetter extends Command {
  public imageName: String;

  constructor(private services: Services) {
    super();
  }

  public setImageName(imageName) {
    this.imageName = imageName;
  }

  public getImageName() {
    return this.imageName;
  }

  //  Override Method
  public async execute() {
    const response = await this.services.showRepository.getShowBanners(
      this.getImageName(),
    );
    return response;
  }
}
