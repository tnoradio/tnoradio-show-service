import { ShowEpisodeTag } from "../../domain/entities/ShowEpisodeTag";
import { ShowRepository } from "../../domain/services/show.service.repository";
import command from "../command";

interface services {
    showRepository: ShowRepository;
}

export default class UpdateEpisodeTags extends command {
    private episodeId: number;
    private tags: ShowEpisodeTag[];
    
    constructor(private services: services) {
        super();
     }

    getEpisodeId() {
        return this.episodeId;
    }

    setEpisodeId(episodeId: number) {
        this.episodeId = episodeId;
    }

    getTags() {
        return this.tags;
    }

    setTags(tags: ShowEpisodeTag[]) {
        this.tags = tags;
    }

      async execute() {
        const updatedTags = await this.services.showRepository.updateEpisodeTags(this.getEpisodeId(), this.getTags());
        return updatedTags;
    }
}