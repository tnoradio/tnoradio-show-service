import { ShowAdvertiser } from "../../domain/entities/ShowAdvertiser";
import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class UpdateShowAdvertiser extends Command {
  public updateValues: any;

  constructor(private services: Services) {
    super();
  }

  public setUpdateValues(updateValues) {
    this.updateValues = updateValues;
  }

  public getUpdateValues() {
    return this.updateValues;
  }

  //  Override Method
  public async execute() {
    const response = await this.services.showRepository.updateAdvertiser(
      this.getUpdateValues(),
    );
    return response;
  }
}
