import Command from "../command";

export class ShowsHealthGetter extends Command {
  constructor() {
    super();
  }

  public async execute() {
    const response = "Shows is Ok.";
    return response;
  }
}
