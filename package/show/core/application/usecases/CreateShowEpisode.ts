import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export interface EpisodeData {
  episode: string;
  episode_number: number;
  title: string;
  description: string;
  url: string;
  tags: string[];
  season: number;
  showId: number;
}

export default class CreateShowEpisode extends Command {
  private episodeData: EpisodeData;

  constructor(private services: Services) {
    super();
  }

  public getEpisodeData(): EpisodeData {
    return this.episodeData;
  }

  public setEpisodeData(episodeData: EpisodeData) {
    this.episodeData = episodeData;
  }

  async execute() {
    try {
      const res = await this.services.showRepository.createShowEpisode(
        this.getEpisodeData(),
      );
      return res;
    } catch (error) {
      return error;
    }
  }
}
