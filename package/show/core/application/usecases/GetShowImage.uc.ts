import { ShowRepository } from "../../domain/services/show.service.repository";
import Command from "../command";

interface Services {
  showRepository: ShowRepository;
}

export class GetShowImage extends Command {
  public showSlug: string;
  public imageName: string;

  constructor(private services: Services) {
    super();
  }

  public setShowSlug(showSlug: string) {
    this.showSlug = showSlug;
  }

  public setImageName(imageName: string) {
    this.imageName = imageName;
  }

  public getShowSlug() {
    return this.showSlug;
  }

  //  Override Method
  public async execute() {
    console.log(this.imageName, this.showSlug);
    const response = await this.services.showRepository.getShowImage(
      this.imageName,
      this.showSlug,
    );
    return response;
  }
}
