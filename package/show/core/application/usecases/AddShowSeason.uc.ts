import { ShowRepository } from "../../domain/services/show.service.repository";
import command from "../command";

interface services {
    showRepository: ShowRepository;
}

export default class AddShowSeason extends command {
    private  showId: number;
    private season_number: number;
    
    constructor(private services: services) {
        super();
     }

    getShowId() {
        return this.showId;
    }

    setShowId(showId: number) {
        this.showId = showId;
    }

    getSeasonNumber() {
        return this.season_number;
    }

    setSeason(season_number: number) {
        this.season_number = season_number;
    }

      async execute() {
        const seasonToAdd = await this.services.showRepository.addSeason(this.getShowId(), this.getSeasonNumber());
        return seasonToAdd;
    }
}