import { ShowEpisodeTag } from "../../domain/entities/ShowEpisodeTag";
import { ShowRepository } from "../../domain/services/show.service.repository";
import command from "../command";

interface Services {
  showRepository: ShowRepository;
}

export default class AddEpisodeTag extends command {
    private tag: ShowEpisodeTag;

    constructor(private services: Services) {
        super();
    }

    public getTag() {
        return this.tag;
    }

    public setTag(value: ShowEpisodeTag) {
        this.tag = value;
    }

    async execute() {
        try {
            const tag = await this.services.showRepository.addEpisodeTag(this.tag);
            return tag;
        } catch (error) {
            throw error;
        }
    }
}