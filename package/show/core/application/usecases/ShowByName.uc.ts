import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class ShowByName extends Command {
  public _nameShow: string;

  constructor(private services: Services) {
    super();
  }

  public setNameShow(id) {
    this._nameShow = id;
  }

  public getNameShow() {
    return this._nameShow;
  }

  //  Override Method
  public async execute() {
    const response = await this.services.showRepository.getShowByName(
      this._nameShow,
    );
    return response;
  }
}
