import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class ShowAdvertisers extends Command {
  showId: string;
  constructor(private services: Services) {
    super();
  }

  public setShowId(showId: string) {
    this.showId = showId;
  }

  public getShowId() {
    return this.showId;
  }

  //  Override Method
  public async execute() {
    const response = await this.services.showRepository.getShowAdvertisers(
      this.getShowId(),
    );
    return response;
  }
}
