import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";
import { ShowEpisode } from "../../domain/entities/ShowEpisode";

interface Services {
  showRepository: ShowRepository;
}

export default class UpdateShowEpisode extends Command {
  private episode: ShowEpisode;
  constructor(private services: Services) {
    super();
  }

  public setEpisode(episode: ShowEpisode) {
    this.episode = episode;
  }

  public getEpisode() {
    return this.episode;
  }

  async execute() {
    const response = await this.services.showRepository.updateShowEpisode(
      this.getEpisode(),
    );
    return response;
  }
}
