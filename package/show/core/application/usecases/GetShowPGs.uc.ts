import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class ShowPGs extends Command {
  constructor(private services: Services) {
    super();
  }

  //  Override Method
  public async execute() {
    const response = await this.services.showRepository.getShowPGs();
    return response;
  }
}
