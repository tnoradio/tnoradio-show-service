import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";
import { ShowImage } from "../../domain/entities/showImage";

/**
 * Uploads image to database.
 */
export class UpdateImageInDB extends Command {
  private _repository: ShowRepository;
  private image: ShowImage;
  private slug: String;
  private name: String;
  private url: String;

  constructor(_repository: ShowRepository) {
    super();
    this._repository = _repository;
  }

  public SetImage(image) {
    this.image = image;
  }

  public SetName(name: String) {
    this.name = name;
  }

  public SetSlug(slug: String) {
    this.slug = slug;
  }

  public SetUrl(url: String) {
    this.url = url;
  }

  public getImage() {
    return this.image;
  }

  public getName() {
    return this.name;
  }

  public getSlug() {
    return this.slug;
  }

  public getUrl() {
    return this.url;
  }

  public async execute() {
    const response = await this._repository.updateShowImage(
      this.getImage(),
      this.getSlug(),
      this.getName(),
      this.getUrl(),
    );
    return response;
  }
}
