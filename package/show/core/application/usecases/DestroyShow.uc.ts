import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class DestroyShow extends Command {
  private _idShow: number;

  constructor(private services: Services) {
    super();
  }

  public setIdShow(id: number) {
    this._idShow = id;
  }

  public getIdShow() {
    return this._idShow;
  }

  public async execute() {
    const response = await this.services.showRepository.destroy(this._idShow);
    return response;
  }
}
