import { Show } from "../../domain/entities/Show";
import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class ShowUpdateById extends Command {
  public updateValues: any;

  constructor(private services: Services) {
    super();
  }

  public setUpdateValues(updateValues) {
    this.updateValues = updateValues;
  }

  public getUpdateValues() {
    return this.updateValues;
  }

  //  Override Method
  public async execute() {
    const response = await this.services.showRepository.updateShow(
      this.getUpdateValues(),
    );
    return response;
  }
}
