import { Show } from "../../domain/entities/Show";
import Command from "../command";
import { ShowRepository } from "../../domain/services/show.service.repository";

export interface Services {
  showRepository: ShowRepository;
}

export class ShowAuthorizedByUserId extends Command {
  public _idUser: string;

  constructor(private services: Services) {
    super();
  }

  public setIdUser(id) {
    this._idUser = id;
  }

  public getIdUser() {
    return this._idUser;
  }

  //  Override Method
  public async execute() {
    const response =
      await this.services.showRepository.getShowsAuthorizedByUserId(
        this._idUser,
      );
    return response;
  }
}
