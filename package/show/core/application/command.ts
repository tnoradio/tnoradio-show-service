/**
 *
 * @author Elberg
 */

export default abstract class Command {
  public abstract async execute();
}
