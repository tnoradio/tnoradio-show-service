import { Show } from "../domain/entities/Show";

export function recomendedShows(
  shows: Show[],
  genders: string[],
  genres: string[],
  target: string[],
  age_ranges: string[],
) {
  var showSum = 0;
  var showList = [];
  shows.forEach((show) => {
    show.target_gender !== undefined &&
      show.target_gender !== null &&
      show.target_gender.forEach((target_gender) => {
        if (genders.includes(target_gender)) showSum++;
      });
    show.target !== undefined &&
      show.target !== null &&
      show.target.forEach((showTarget) => {
        if (target.includes(showTarget)) showSum++;
      });
    show.age_range !== null &&
      show.age_range !== undefined &&
      show.age_range.forEach((age_range) => {
        if (age_ranges.includes(age_range)) {
          console.log("AGE RANGE ESTA ", age_range, age_ranges);
          showSum++;
        }
      });
    if (genres.includes(show.genre)) {
      console.log(show.name);
      console.log("SHOW GENRE ESTA ", genres, show.genre);
      showSum = showSum + 100;
    }
    showList.push({ show: show, showSum: showSum });
    showSum = 0;
  });

  return showList.sort((a, b) => b.showSum - a.showSum);
}
