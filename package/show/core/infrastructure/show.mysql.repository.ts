import { Show } from "../domain/entities/Show";
import { ShowRepository } from "../domain/services/show.service.repository";
import { getManager } from "typeorm";
import * as translate from "../../utils/translateDay";
import { ShowGenre } from "../domain/entities/ShowGenre";
import { ShowPGClassification } from "../domain/entities/ShowPGClassification";
import { ShowType } from "../domain/entities/ShowType";
import { ShowImage } from "../domain/entities/showImage";
import { ShowAdvertiser } from "../domain/entities/ShowAdvertiser";
import * as calculator from "../infrastructure/recommended.show.calculator";
import { ShowSubGenre } from "../domain/entities/ShowSubGenre";
import { ShowSeason } from "../domain/entities/ShowSeason";
import { ShowEpisode } from "../domain/entities/ShowEpisode";
import { ShowEpisodeTag } from "../domain/entities/ShowEpisodeTag";

export class ShowMysqlRepository implements ShowRepository {
  async createShowEpisode(episode: any): Promise<ShowEpisode> {
    try {
      const seasonRepository = getManager().getRepository(ShowSeason);

      // Find the season by season_number and showId
      let savedSeason = await seasonRepository.findOne({
        where: {
          season_number: episode.season,
          show: { id: episode.showId }, // Assuming `show` is a relation in ShowSeason
        },
        relations: ["show"], // Include related show entity
      });

      // If the season doesn't exist, create and save it
      if (!savedSeason) {
        const showRepository = getManager().getRepository(Show);
        const show = await showRepository.findOne({
          where: { id: episode.showId },
        });

        if (!show) {
          throw new Error(`Show with ID ${episode.showId} not found`);
        }

        const newSeason = seasonRepository.create({
          season_number: episode.season,
          show: show,
          episodes: [],
        });
        savedSeason = await seasonRepository.save(newSeason);
      }

      // Check if an episode with the same number, season, and showId already exists
      const episodeRepository = getManager().getRepository(ShowEpisode);
      const existingEpisode = await episodeRepository.findOne({
        where: {
          episode_number: episode.episode_number,
          season: savedSeason,
        },
      });

      if (existingEpisode) {
        throw new Error(
          `Episode ${episode.episode_number} for season ${episode.season} and show ID ${episode.showId} already exists.`,
        );
      }

      // Associate the episode with the saved season
      episode.season = savedSeason;

      // Save the episode
      const savedEpisode = await episodeRepository.save(episode);

      return savedEpisode;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async saveSeason(season: ShowSeason): Promise<ShowSeason> {
    try {
      const entityManager = getManager().getRepository(ShowSeason);
      const savedSeason = await entityManager.save(season);
      return savedSeason;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async getPodcasts(): Promise<Show[]> {
    try {
      const shows = await getManager()
        .getRepository(Show)
        .createQueryBuilder("show")
        .leftJoinAndSelect("show.showSchedules", "showSchedule")
        .leftJoinAndSelect("show.hosts", "hosts")
        .leftJoinAndSelect("show.socials", "socials")
        .leftJoinAndSelect("show.videos", "videos")
        .leftJoinAndSelect("show.seasons", "seasons")
        .leftJoinAndSelect("seasons.episodes", "episodes")
        .where("show.isPodcast = true")
        .getMany();
      return shows;
    } catch (error) {
      return error;
    }
  }

  async getMatchingShows(
    genders: string[],
    genres: string[],
    target: string[],
    age_ranges: string[],
  ): Promise<Show[]> {
    try {
      const enabledShows = await this.getOnlyEnabledShows();
      const shows = calculator.recomendedShows(
        enabledShows,
        genders,
        genres,
        target,
        age_ranges,
      );
      return shows;
    } catch (error) {
      return error;
    }
  }

  destroy(id: number): Promise<any | Error> {
    try {
      return getManager().getRepository(Show).delete(id);
    } catch (error) {
      return error;
    }
  }

  destroyAdvertiser(id: number): Promise<any | Error> {
    try {
      return getManager().getRepository(ShowAdvertiser).delete(id);
    } catch (error) {
      return error;
    }
  }

  async getShowGenres(): Promise<ShowGenre[]> {
    try {
      let showGenres = await getManager().getRepository(ShowGenre).find();
      return showGenres;
    } catch (error) {
      return error;
    }
  }

  async getShowAdvertisers(showId: string): Promise<ShowAdvertiser[]> {
    try {
      const showAdvertisers = await getManager()
        .getRepository(ShowAdvertiser)
        .createQueryBuilder("advertiser")
        .innerJoin("advertiser.shows", "show")
        .where("show.id = :showId", { showId })
        .select(["advertiser"])
        .getMany();

      return showAdvertisers;
    } catch (error) {
      console.error("Error fetching show advertisers:", error);
      throw error; // Re-throw the error for handling at a higher level
    }
  }

  async getAdvertisers(): Promise<ShowAdvertiser[]> {
    try {
      const showAdvertisers = await getManager()
        .getRepository(ShowAdvertiser)
        .find();
      return showAdvertisers;
    } catch (error) {
      console.error("Error fetching advertisers:", error);
      throw error;
    }
  }

  async getShowPGs(): Promise<ShowPGClassification[]> {
    try {
      let showPGs = await getManager()
        .getRepository(ShowPGClassification)
        .find();
      return showPGs;
    } catch (error) {
      return error;
    }
  }

  async getShowTypes(): Promise<ShowType[]> {
    try {
      let showTypes = await getManager().getRepository(ShowType).find();
      return showTypes;
    } catch (error) {
      return error;
    }
  }

  async updateShow(updateValues: any): Promise<Show | Error> {
    try {
      let showToUpdate =
        updateValues.field === "seasons" ||
        updateValues.field === "showSchedules" ||
        updateValues.field === "socials" ||
        updateValues.field === "producers" ||
        updateValues.field === "hosts" ||
        updateValues.field === "genres" ||
        updateValues.field === "subgenres" ||
        updateValues.field === "images" ||
        updateValues.field === "advertisers" ||
        updateValues.field === "colors" ||
        updateValues.videos === "videos"
          ? await getManager()
              .getRepository(Show)
              .findOne({
                where: {
                  id: updateValues.id,
                },
                relations: [updateValues.field],
              })
          : await getManager()
              .getRepository(Show)
              .findOne({
                where: {
                  id: updateValues.id,
                },
              });

      let updatedShow: Show = createShowToUpdate(showToUpdate, updateValues);
      let res = await getManager().getRepository(Show).save(updatedShow);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async updateAdvertiser(updateValues: any): Promise<ShowAdvertiser | Error> {
    try {
      let advertiserToUpdate = await getManager()
        .getRepository(ShowAdvertiser)
        .findOne({
          where: {
            id: updateValues.id,
          },
        });

      let updatedAdvertiser: ShowAdvertiser = createAdvertiserToUpdate(
        advertiserToUpdate,
        updateValues,
      );

      let res = await getManager()
        .getRepository(ShowAdvertiser)
        .save(updatedAdvertiser);
      return res;
    } catch (error) {
      return error;
    }
  }

  async updateShowImage(
    image,
    slug: string,
    name: string,
    url: string,
  ): Promise<any> {
    try {
      const res = getManager().update(
        ShowImage,
        { showSlug: slug, name: name },
        image,
      );

      return res;
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  fetchShowSeasonsWithEpisodes(showId: number): Promise<ShowSeason[]> {
    try {
      return getManager()
        .getRepository(ShowSeason)
        .find({
          where: {
            show: { id: showId },
          },
          relations: ["episodes"],
        });
    } catch (error) {
      return error;
    }
  }

  getShowsAuthorizedByUserId(id: string): Promise<Show[] | Error> {
    try {
      return getManager()
        .getRepository(Show)
        .find({
          where: {
            fk_user_id: id,
          },
        });
    } catch (error) {
      return error;
    }
  }

  getShowImage(name: String, slug: String): Promise<ShowImage> {
    console.log("getShowImage", name, slug);
    try {
      return getManager()
        .getRepository(ShowImage)
        .findOne({
          where: {
            name: name,
            showSlug: slug,
          },
        });
    } catch (error) {
      return error;
    }
  }

  getAdImage(name: String, slug: String): Promise<ShowAdvertiser> {
    try {
      return getManager()
        .getRepository(ShowAdvertiser)
        .findOne({
          where: {
            name: name,
            showSlug: slug,
          },
        });
    } catch (error) {
      return error;
    }
  }

  getShowByName(nameShow: string): Promise<Show | Error> {
    try {
      return getManager()
        .getRepository(Show)
        .findOne({
          where: {
            name: nameShow,
          },
        });
    } catch (error) {
      return error;
    }
  }

  async getShowBySlug(slug: string): Promise<Show> {
    try {
      let show = await getManager()
        .getRepository(Show)
        .createQueryBuilder("show")
        .leftJoinAndSelect("show.showSchedules", "showSchedule")
        .leftJoinAndSelect("show.hosts", "hosts")
        .leftJoinAndSelect("show.socials", "socials")
        .leftJoinAndSelect("show.producers", "producers")
        .leftJoinAndSelect("show.genres", "genres")
        .leftJoinAndSelect("show.subgenres", "subgenres")
        .leftJoinAndSelect("show.videos", "videos")
        .leftJoinAndSelect("show.seasons", "seasons")
        .leftJoinAndSelect("show.colors", "colors")
        .leftJoinAndSelect("seasons.episodes", "episodes")
        .leftJoinAndSelect("episodes.tags", "tags")
        .where("show.showSlug = :slug", { slug })
        .getOne();

      show.seasons?.forEach((season) => {
        season.episodes.sort((a, b) => a.episode_number - b.episode_number);
      });

      return show;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async getOnlineShowByTime(time: Date): Promise<Show> {
    try {
      var now = new Date(time);
      now.setTime(now.getTime() + now.getTimezoneOffset() * 60 * 1000);
      var day = translate.translateDay(now.getDay());
      console.log(day);
      const shows = await this.getShowsOnWeekday(day);
      const show = this.getShow(shows, now);
      return show;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async getEpisodeTags(): Promise<ShowEpisodeTag[]> {
    try {
      const showEpisodeTags = await getManager()
        .getRepository(ShowEpisodeTag)
        .find();
      return showEpisodeTags;
    } catch (error) {
      return error;
    }
  }

  async saveEpisodeTag(episodeTag: ShowEpisodeTag): Promise<ShowEpisodeTag> {
    try {
      const savedEpisodeTag = await getManager()
        .getRepository(ShowEpisodeTag)
        .save(episodeTag);
      return savedEpisodeTag;
    } catch (error) {
      return error;
    }
  }

  async updateEpisodeTags(episodeId: number, newTags: ShowEpisodeTag[]): Promise<ShowEpisodeTag[]> {
    try {
      const entityManager = getManager();
      const episodeTagRepository = entityManager.getRepository(ShowEpisodeTag);
      const episodeRepository = entityManager.getRepository(ShowEpisode); // Assuming ShowEpisode is the entity representing episodes

      // Fetch the episode and its current tags
      const episode = await episodeRepository.findOne(episodeId, { relations: ['tags'] });
      if (!episode) throw new Error('Episode not found');

      // Remove tags that are not in the newTags array
      episode.tags = episode.tags.filter(tag => newTags.some(newTag => newTag.id === tag.id));

      // Add or update the tags
      newTags.forEach(newTag => {
        const existingTag = episode.tags.find(tag => tag.id === newTag.id);
        if (existingTag) {
          // Update existing tag
          existingTag.name = newTag.name;
        } else {
          // Add new tag
          episode.tags.push(newTag);
        }
      });

      // Save the updated episode with its tags
      await episodeRepository.save(episode);

      return episode.tags;
    } catch (error) {
      throw new Error(`Error updating episode tags: ${error.message}`);
    }
  }



async addTagToEpisode(
  newTag: ShowEpisodeTag,
  episodeId: number
): Promise<ShowEpisode> {
  try {
    const entityManager = getManager();
    const episodeRepository = entityManager.getRepository(ShowEpisode);
    const tagRepository = entityManager.getRepository(ShowEpisodeTag);

    const episode = await episodeRepository.findOne({ 
      where: { id: episodeId },
      relations: ['tags'] 
    });

    if (!episode) {
      throw new Error('Episode not found');
    }

    let tag = await tagRepository.findOne({ where: { name: newTag.name } });

    if (!tag) {
      tag = tagRepository.create({ name: newTag.name });
      await tagRepository.save(tag);
    }

    if (episode.tags.some(t => t.id === tag.id)) {
      throw new Error('Episode already has this tag');
    }

    episode.tags.push(tag);

    await episodeRepository.save(episode);

    return episode;
  } catch (error) {
    throw new Error(`Failed to add tag to episode: ${error.message}`);
  }
}

  async saveImage(image): Promise<ShowImage> {
    try {
      const imageRes = getManager().getRepository(ShowImage).save(image);

      if (imageRes == null) return null;
      else return imageRes;
    } catch (err) {
      return err;
    }
  }

  async saveAdvertiser(advertiser): Promise<ShowAdvertiser> {
    try {
      const imageRes = getManager()
        .getRepository(ShowAdvertiser)
        .save(advertiser);

      if (imageRes == null) return null;
      else return imageRes;
    } catch (err) {
      return err;
    }
  }

  async getShowsOnWeekday(weekday: string): Promise<Show[]> {
    try {
      const shows = await getManager()
        .getRepository(Show)
        .createQueryBuilder("show")
        .leftJoinAndSelect("show.showSchedules", "showSchedule")
        .where("show.isEnabled = true")
        .getMany();
      return shows;
    } catch (error) {
      return error;
    }
  }

  async getShowById(showId: number): Promise<Show> {
    try {
      const show = await getManager()
        .getRepository(Show)
        .createQueryBuilder("show")
        .leftJoinAndSelect("show.showSchedules", "showSchedule")
        .leftJoinAndSelect("show.hosts", "hosts")
        .leftJoinAndSelect("show.socials", "socials")
        .leftJoinAndSelect("show.producers", "producers")
        .leftJoinAndSelect("show.genres", "genres")
        .leftJoinAndSelect("show.subgenres", "subgenres")
        .leftJoinAndSelect("show.videos", "videos")
        .leftJoinAndSelect("show.seasons", "seasons")
        .leftJoinAndSelect("show.colors", "colors")
        .leftJoinAndSelect("seasons.episodes", "episodes")
        .leftJoinAndSelect("episodes.tags", "tags")
        .where("show.id = :showId", { showId })
        .getOne();
     
        show.seasons?.forEach((season) => {
          season.episodes.sort((a, b) => a.episode_number - b.episode_number);
        });
       return show;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async createShow(show: Show): Promise<Show | Error> {
    try {
      const entityManager = getManager().getRepository(Show);
      const savedSchedules = await Promise.all(
        show.showSchedules.map(async (schedule) => {
          const savedSchedule = await entityManager.save(schedule); // Save each schedule
          return savedSchedule;
        }),
      );
      show.showSchedules = savedSchedules; // Update showSchedules with saved entities
      console.log("showSchedules");

      const savedProducers = await Promise.all(
        show.producers.map(async (producer) => {
          // Assuming user object is already saved elsewhere
          return producer; // No need to save user again
        }),
      );
      show.producers = savedProducers; // Update producers with existing user entities
      const savedHosts = await Promise.all(
        show.hosts.map(async (host) => {
          // Assuming user object is already saved elsewhere
          return host; // No need to save user again
        }),
      );
      show.hosts = savedHosts;

      const genreEntities = show.genres.map((genre) => genre.id);
      const existingGenres = await getManager()
        .getRepository(ShowGenre)
        .findByIds(genreEntities);
      show.genres = existingGenres;

      const subGenreEntities = show.subgenres.map((subgenre) => subgenre.id);
      const existingSubgenres = await getManager()
        .getRepository(ShowSubGenre)
        .findByIds(subGenreEntities);
      show.subgenres = existingSubgenres;

      return await entityManager.save(show);
    } catch (error) {
      console.log("sql error", error);
      return error;
    }
  }

  getAllShows(): Promise<Error | Show[]> {
    try {
      return getManager()
        .getRepository(Show)
        .createQueryBuilder("show")
        .leftJoinAndSelect("show.showSchedules", "showSchedule")
        .leftJoinAndSelect("show.hosts", "hosts")
        .leftJoinAndSelect("show.socials", "socials")
        .leftJoinAndSelect("show.producers", "producers")
        .leftJoinAndSelect("show.genres", "genres")
        .leftJoinAndSelect("show.subgenres", "subgenres")
        .leftJoinAndSelect("show.videos", "videos")
        .leftJoinAndSelect("show.seasons", "seasons")
        .leftJoinAndSelect("show.colors", "colors")
        .leftJoinAndSelect("seasons.episodes", "episodes")
        .leftJoinAndSelect("episodes.tags", "tags")
        .getMany();
    } catch (error) {
      return error;
    }
  }

  async getShowBanners(imageName: String): Promise<ShowImage[]> {
    try {
      const photoRepository = getManager().getRepository(ShowImage);
      const photos1 = getManager()
        .createQueryBuilder(Show, "show")
        .select("show_image.*")
        .leftJoinAndSelect(
          ShowImage,
          "show_image",
          "show_image.showSlug = show.showSlug",
        )

        .where("show.isEnabled = TRUE")
        .andWhere("show_image.name = :name", { name: imageName })
        .getRawMany();
      return photos1;
    } catch (error) {
      return error;
    }
  }

  async addEpisodeTag(newTag: ShowEpisodeTag): Promise<ShowEpisodeTag> {
    console.log(newTag);
    try {
      const entityManager = getManager();
      const episodeTagRepository = entityManager.getRepository(ShowEpisodeTag);

      let tag = await episodeTagRepository.findOne({ where: { name: newTag.name } });

    if (!tag) {
      tag = episodeTagRepository.create({ name: newTag.name });
      await episodeTagRepository.save(tag);
    }

      return tag;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async addSeason(showId: number, season_number: number): Promise<ShowSeason> {
    try {
      const seasonRepository = getManager().getRepository(ShowSeason);
      const showRepository = getManager().getRepository(Show);

      const show = await showRepository.findOne({ where: { id: showId } });
      if (!show) {
        throw new Error(`Show with ID ${showId} not found`);
      }

      let season = await seasonRepository.findOne({
        where: { season_number: season_number, show: { id: showId } },
        relations: ["show"],
      });

      if (!season) {
        const newSeason = seasonRepository.create({
          season_number: season_number,
          show: show,
          episodes: [],
        });
        season = await seasonRepository.save(newSeason);
      }

      return season;
    } catch (error) {
      console.error("Error adding season:", error);
      throw new Error(`Could not add season: ${error.message}`);
    }
  }

  getOnlyEnabledShows(): Promise<Show[]> {
    try {
      const shows = getManager()
        .getRepository(Show)
        .find({
          relations: [
            "showSchedules",
            "hosts",
            "socials",
            "videos",
            "genres",
            "subgenres",
            "seasons",
            "seasons.episodes",
          ],
          where: { isEnabled: true },
        });
      return shows;
    } catch (error) {
      return error;
    }
  }

  getEnabledShows(): Promise<Show[]> {
    try {
      const shows = getManager()
        .getRepository(Show)
        .find({
          relations: [
            "showSchedules",
            "hosts",
            "colors",
            "socials",
            "videos",
            "seasons",
            "seasons.episodes",
            "genres",
            "subgenres",
          ],
          where: { isEnabled: true },
        });
      return shows;
    } catch (error) {
      return error;
    }
  }

  getShowsByGenre(genre: string): Promise<Show[]> {
    try {
      const shows = getManager()
        .getRepository(Show)
        .find({
          where: { genre: genre },
        });
      return shows;
    } catch (error) {
      return error;
    }
  }

  getShow(shows: Show[], now: Date): Show {
    let resShow: Show;
    const date = new Date();
    const newNow = date.getTime();

    shows.forEach((show) => {
      show.showSchedules.forEach((schedule) => {
        if (schedule.weekDay === translate.translateDay(now.getDay())) {
          let startHour, endHour;
          let startMinute, endMinute;
          let scheduleStartTime: Date = new Date();
          let scheduleEndTime = new Date();

          try {
            startHour = translate.getTime(schedule.startTime.toString())[0];
            startMinute = translate.getTime(schedule.startTime.toString())[1];
            endHour = translate.getTime(schedule.endTime.toString())[0];
            endMinute = translate.getTime(schedule.endTime.toString())[1];
            scheduleStartTime.setHours(startHour);
            scheduleStartTime.setMinutes(startMinute);
            scheduleEndTime.setHours(endHour);
            scheduleEndTime.setMinutes(endMinute);

            if (
              scheduleStartTime.getTime() <= newNow &&
              newNow <= scheduleEndTime.getTime()
            ) {
              console.log("in time ", show.showSlug);
              resShow = show;
            }
          } catch (e) {
            console.log(e);
          }
        }
      });
    });
    return resShow;
  }

  async createShowSubGenre(
    genreId: number,
    showSubGenre: string,
  ): Promise<ShowSubGenre | Error> {
    try {
      const subgenre = await getManager()
        .getRepository(ShowSubGenre)
        .save({ subgenre: showSubGenre });
      const genre = await getManager()
        .getRepository(ShowGenre)
        .findOne(genreId, { relations: ["subgenres"] });
      genre.subgenres.push(subgenre);
      await getManager().getRepository(ShowGenre).save(genre);
      return subgenre;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async updateShowEpisode(showEpisode: ShowEpisode): Promise<ShowEpisode> {
    try {
      const updatedShowEpisode = await getManager()
        .getRepository(ShowEpisode)
        .save(showEpisode);
      return updatedShowEpisode;
    } catch (error) {
      return error;
    }
  }

  async getShowSubGenres(): Promise<ShowSubGenre[]> {
    try {
      const showSubGenres = await getManager()
        .getRepository(ShowSubGenre)
        .find();
      return showSubGenres;
    } catch (error) {
      return error;
    }
  }

  async getShowsBySubGenre(subGenre: string): Promise<Show[]> {
    try {
      const shows = await getManager()
        .getRepository(Show)
        .createQueryBuilder("show")
        .leftJoinAndSelect("show.showSchedules", "showSchedule")
        .where("show.isEnabled = true")
        .andWhere("show.showSubGenre = :subGenre", { subGenre })
        .getMany();
      return shows;
    } catch (error) {
      return error;
    }
  }

  async getShowsByGGenre(genre: string): Promise<Show[]> {
    try {
      const shows = await getManager()
        .getRepository(Show)
        .createQueryBuilder("show")
        .leftJoinAndSelect("show.showSchedules", "showSchedule")
        .where("show.isEnabled = true")
        .andWhere("show.genre = :genre", { genre })
        .getMany();
      return shows;
    } catch (error) {
      return error;
    }
  }

  async getShowsByTodaysWeekday(): Promise<Show[]> {
    try {
      const now = new Date();
      const dayOfWeek = translate.translateDay(now.getDay());

      const shows = await getManager()
        .getRepository(Show)
        .createQueryBuilder("show")
        .leftJoinAndSelect("show.showSchedules", "showSchedule")
        .where("showSchedule.weekDay = :dayOfWeek", { dayOfWeek })
        .andWhere("show.isActive = true")
        .getMany();

      const filteredShows = shows.filter((show) =>
        show.showSchedules.some((schedule) => {
          return compareTimestampWithCurrentHour(schedule.startTime);
        }),
      );

      return sortedShows(filteredShows);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async getGenresWithShows(): Promise<ShowGenre[]> {
    try {
      const qb = getManager()
        .getRepository(ShowGenre)
        .createQueryBuilder("genre");
      const showGenres = await qb
        .leftJoinAndSelect("genre.shows", "show", "show.isEnabled = true")
        .select(["genre.id", "genre.genre", "show.showSlug", "show.name"])
        .getMany();
      return showGenres;
    } catch (error) {
      return error;
    }
  }
}

function createShowToUpdate(
  showToUpdate: Show,
  updateValues: { id: number; field: string; value: any },
) {
  const show = {
    id: updateValues.id,
    name:
      updateValues.field === "name" ? updateValues.value : showToUpdate.name,
    genre:
      updateValues.field === "genre" ? updateValues.value : showToUpdate.genre,
    synopsis:
      updateValues.field === "synopsis"
        ? updateValues.value
        : showToUpdate.synopsis,
    type:
      updateValues.field === "type" ? updateValues.value : showToUpdate.type,
    showSchedules:
      updateValues.field === "showSchedules"
        ? updateValues.value
        : showToUpdate.showSchedules,
    isActive:
      updateValues.field === "isActive"
        ? updateValues.value
        : showToUpdate.isActive,
    isEnabled:
      updateValues.field === "isEnabled"
        ? updateValues.value
        : showToUpdate.isEnabled,
    email:
      updateValues.field === "email" ? updateValues.value : showToUpdate.email,
    pgClassification:
      updateValues.field === "pgClassification"
        ? updateValues.value
        : showToUpdate.pgClassification,
    initDate:
      updateValues.field === "initDate"
        ? updateValues.value
        : showToUpdate.initDate,
    producers:
      updateValues.field === "producers"
        ? updateValues.value
        : showToUpdate.producers,
    showSlug:
      updateValues.field === "showSlug"
        ? updateValues.value
        : showToUpdate.showSlug,
    socials:
      updateValues.field === "socials"
        ? updateValues.value
        : showToUpdate.socials,
    hosts:
      updateValues.field === "hosts" ? updateValues.value : showToUpdate.hosts,
    images:
      updateValues.field === "images"
        ? updateValues.value
        : showToUpdate.images,
    advertisers:
      updateValues.field === "advertisers"
        ? updateValues.value
        : showToUpdate.advertisers,
    colors:
      updateValues.field === "colors"
        ? updateValues.value
        : showToUpdate.colors,
    genres:
      updateValues.field === "genres"
        ? updateValues.value
        : showToUpdate.genres,
    subgenres:
      updateValues.field === "subgenres"
        ? updateValues.value
        : showToUpdate.subgenres,
    isDeleted:
      updateValues.field === "isDeleted"
        ? updateValues.value
        : showToUpdate.isDeleted,
    isPodcast:
      updateValues.field === "isPodcast"
        ? updateValues.value
        : showToUpdate.isPodcast,
    target:
      updateValues.field === "target"
        ? updateValues.value
        : showToUpdate.target,
    age_range:
      updateValues.field === "age_range"
        ? updateValues.value
        : showToUpdate.age_range,
    target_gender:
      updateValues.field === "target_gender"
        ? updateValues.value
        : showToUpdate.target_gender,
    videos:
      updateValues.field === "videos"
        ? updateValues.value
        : showToUpdate.videos,
    updatedBy: "",
    seasons:
      updateValues.field === "seasons"
        ? updateValues.value
        : showToUpdate.seasons,
    likes:
      updateValues.field === "likes" ? updateValues.value : showToUpdate.likes,
    dislikes:
      updateValues.field === "dislikes"
        ? updateValues.value
        : showToUpdate.dislikes,
    views:
      updateValues.field === "views" ? updateValues.value : showToUpdate.views,
    rating:
      updateValues.field === "rating"
        ? updateValues.value
        : showToUpdate.rating,
  };

  return show;
}

function createAdvertiserToUpdate(advertiserToUpdate, updateValues) {
  const advertiser = {
    id: updateValues.id,
    name:
      updateValues.field === "name"
        ? updateValues.value
        : advertiserToUpdate.name,
    website:
      updateValues.field === "website"
        ? updateValues.value
        : advertiserToUpdate.website,
    shows:
      updateValues.field === "shows"
        ? updateValues.value
        : advertiserToUpdate.shows,
    image:
      updateValues.field === "image"
        ? updateValues.value
        : advertiserToUpdate.image,
  };

  return advertiser;
}

function compareTimestampWithCurrentHour(timestampString) {
  // Parse the timestamp string to extract the hour
  const [hours, minutes, seconds] = timestampString.split(":");
  const timestampHour = parseInt(hours, 10);

  // Get the current date and time
  const now = new Date();
  const currentHour = now.getHours();

  // Compare the hours
  return timestampHour >= currentHour;
}

const sortedShows = (shows: Show[]) =>
  shows.sort((a, b) => {
    const scheduleA = a.showSchedules.find((schedule) =>
      compareTimestampWithCurrentHour(schedule.startTime),
    );
    const scheduleB = b.showSchedules.find((schedule) =>
      compareTimestampWithCurrentHour(schedule.startTime),
    );

    if (!scheduleA || !scheduleB) {
      return 0;
    }

    const [hoursA, minutesA] = scheduleA.startTime.toString().split(":");
    const [hoursB, minutesB] = scheduleB.startTime.toString().split(":");

    const timeA = parseInt(hoursA, 10) * 60 + parseInt(minutesA, 10);
    const timeB = parseInt(hoursB, 10) * 60 + parseInt(minutesB, 10);

    return timeA - timeB;
  });
