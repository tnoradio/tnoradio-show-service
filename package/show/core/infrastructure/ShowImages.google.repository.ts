import drive from "../../loaders/google";
import { ShowImageRepository } from "../domain/services/show.images.repository";
const fs = require("fs");
import config from "../../config";

export class GoogleRepository implements ShowImageRepository {
  getShowImage(imageName: any, options: any, res: any) {
    throw new Error("Method not implemented.");
  }

  listDriveFiles() {
    drive.files.list({}, (err, res) => {
      if (err) throw err;
      const files = res.data.files;
      if (files.length) {
        files.map((file) => {
          console.log(file);
        });
      } else {
        console.log("No files found");
      }
    });
  }

  async generatePublicUrl(fileId) {
    try {
      //change file permisions to public.
      await drive.permissions.create({
        fileId: fileId,
        requestBody: {
          role: "reader",
          type: "anyone",
        },
      });

      //obtain the webview and webcontent links
      const result = await drive.files.get({
        fileId: fileId,
        fields: "webViewLink, webContentLink",
      });
      console.log(result.data);
    } catch (error) {
      console.log(error.message);
    }
  }

  //delete file function
  async deleteFile(fileId) {
    try {
      const response = await drive.files.delete({
        fileId: fileId, // file id
      });
      //console.log(response.data, response.status);
    } catch (error) {
      //console.log(error.message);
    }
  }

  //function to upload the file
  async uploadFile(name, mimeType, filePath) {
    console.log("UPLOAD A DRVE");
    console.log(config.parent_folder);
    this.listDriveFiles();
    try {
      const response = await drive.files.create({
        requestBody: {
          name: name, //file name
          mimeType: mimeType,
          parents: [config.parent_folder],
        },
        media: {
          mimeType: mimeType,
          body: fs.createReadStream(filePath),
        },
      });
      this.generatePublicUrl(response.data.id);
      const imageUrl = `https://drive.google.com/uc?id=${response.data.id}`;
      console.log("drive response");
      console.log(imageUrl);
      return imageUrl;
    } catch (error) {
      //report the error message
      console.log("DRIVE ERROR");
      console.log(error.message);
    }
  }
}
