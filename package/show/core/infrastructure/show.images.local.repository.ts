import { ShowImageRepository } from "../domain/services/show.images.repository";

export class ShowImagesLocalRepository implements ShowImageRepository {
  uploadFile(name: any, mimeType: any, filePath: any) {
    throw new Error("Method not implemented.");
  }
  listDriveFiles() {
    throw new Error("Method not implemented.");
  }
  generatePublicUrl(fileId: any) {
    throw new Error("Method not implemented.");
  }
  deleteFile(fileId: any) {
    throw new Error("Method not implemented.");
  }
  getShowImage(imageName, options, res) {
    try {
      return res.sendFile(imageName, options, function (err) {
        if (err) {
          console.log(err);
        } else {
          console.log("Sent:", imageName);
        }
      });
    } catch (e) {
      console.log(e);
    }
  }
}
