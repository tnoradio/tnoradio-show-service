import { Show } from "../entities/Show";
import { ShowAdvertiser } from "../entities/ShowAdvertiser";
import { ShowEpisode } from "../entities/ShowEpisode";
import { ShowEpisodeTag } from "../entities/ShowEpisodeTag";
import { ShowGenre } from "../entities/ShowGenre";
import { ShowImage } from "../entities/showImage";
import { ShowPGClassification } from "../entities/ShowPGClassification";
import { ShowSeason } from "../entities/ShowSeason";
import { ShowSubGenre } from "../entities/ShowSubGenre";
import { ShowType } from "../entities/ShowType";

interface EpisodeData {
  episode: string;
  episode_number: number;
  title: string;
  description: string;
  url: string;
  tags: string[];
  season: number;
  showId: number;
}

export interface ShowRepository {
  createShow(show: Show): Promise<Show | Error>;
  getAllShows(): Promise<Show[] | Error>;
  getEnabledShows(): Promise<Show[] | Error>;
  getShowById(id: number): Promise<Show>;
  getShowByName(nameShow: string): Promise<Show | Error>;
  getShowBySlug(slug: string): Promise<Show>;
  getShowsOnWeekday(weekday: string): Promise<Show[]>;
  getShowsAuthorizedByUserId(id: string): Promise<Show[] | Error>;
  getOnlineShowByTime(time: Date): Promise<Show>;
  getShowGenres(): Promise<ShowGenre[]>;
  getPodcasts(): Promise<Show[]>;
  getShowAdvertisers(showId: string): Promise<ShowAdvertiser[]>;
  getShowPGs(): Promise<ShowPGClassification[]>;
  getShowTypes(): Promise<ShowType[]>;
  updateShow(updateValues: any): Promise<Show | Error>;
  destroy(id: number): Promise<string | Error>;
  destroyAdvertiser(id: number): Promise<string | Error>;
  saveImage(image: ShowImage): Promise<ShowImage>;
  saveAdvertiser(advertiser: ShowAdvertiser): Promise<ShowAdvertiser>;
  updateShowImage(
    image: ShowImage,
    slug: String,
    name: String,
    url: String,
  ): Promise<any>;
  getShowImage(name: String, slug: String): Promise<ShowImage>;
  getAdImage(name: String, slug: String): Promise<ShowAdvertiser>;
  getShowBanners(imageName: String): Promise<ShowImage[]>;
  updateAdvertiser(updateValues: any): Promise<ShowAdvertiser | Error>;
  getShowsByGenre(genre: string): Promise<Show[]>;
  getMatchingShows(
    genders: string[],
    genres: string[],
    target: string[],
    age_ranges: string[],
  ): Promise<Show[]>;
  createShowSubGenre(
    genreId: number,
    showSubGenre: string,
  ): Promise<ShowSubGenre | Error>;
  getShowSubGenres(): Promise<ShowSubGenre[]>;
  getShowsBySubGenre(subGenre: string): Promise<Show[]>;
  getShowsByGGenre(genre: string): Promise<Show[]>;
  getGenresWithShows(): Promise<ShowGenre[]>;
  getShowsByTodaysWeekday(): Promise<Show[]>;
  createShowEpisode(episodeData: EpisodeData): Promise<ShowEpisode>;
  fetchShowSeasonsWithEpisodes(showId: number): Promise<ShowSeason[]>;
  updateShowEpisode(episode: ShowEpisode): Promise<ShowEpisode | Error>;
  addTagToEpisode(tag: ShowEpisodeTag, episodeId: number): Promise<ShowEpisode | Error>;
  getEpisodeTags(): Promise<ShowEpisodeTag[]>;
  addSeason(showId: number, season_number: number): Promise<ShowSeason>;
  addEpisodeTag(tag: ShowEpisodeTag): Promise<ShowEpisodeTag | Error>;
  updateEpisodeTags(episodeId: number, tags: ShowEpisodeTag[]): Promise<ShowEpisodeTag[]>;
}
