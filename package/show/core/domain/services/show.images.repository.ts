export interface ShowImageRepository {
  getShowImage(imageName, options, res): any;
  uploadFile(name, mimeType, filePath);
  listDriveFiles();
  generatePublicUrl(fileId);
  deleteFile(fileId);
}
