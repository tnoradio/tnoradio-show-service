import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Show } from "./Show";

@Entity()
export class ShowSocialMedia {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 100,
    default: "Social Network Name",
  })
  name: string;

  @Column({
    type: "varchar",
    length: 100,
    nullable: true,
  })
  url: string;

  @Column({
    type: "varchar",
    length: 100,
    nullable: true,
  })
  userName: string;

  @ManyToOne((type) => Show, (show) => show.socials, {
    onDelete: "CASCADE",
  })
  show: Show;

  constructor(showId, name, url, userName) {
    this.name = name;
    this.url = url;
    this.userName = userName;
    this.show = showId;
  }
}
