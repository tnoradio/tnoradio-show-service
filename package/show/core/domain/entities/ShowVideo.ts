import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";

import { Show } from "./Show";

@Entity()
export class ShowVideo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 100,
    //nullable: true
    default: "Show name",
  })
  name: string;

  @Column({
    type: "varchar",
    length: 100,
    //nullable: true
    default: "Show description",
  })
  description: string;

  @Column({
    type: "varchar",
    length: 100,
    //nullable: true
    default: "Show url",
  })
  url: string;

  @ManyToOne((type) => Show, (show) => show.videos, {
    onDelete: "CASCADE",
  })
  show: Show;

  constructor(showId, name, description, url) {
    this.name = name;
    this.description = description;
    this.url = url;
    this.show = showId;
  }
}
