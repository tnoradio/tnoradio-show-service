import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";

import { Show } from "./Show";

@Entity()
export class ShowColor {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 100,
    default: "#FFFFFF",
  })
  color: string;

  @Column({
    type: "varchar",
    length: 100,
    default: "#FFFFFF",
  })
  name: string;

  @ManyToOne(() => Show, (show) => show.colors, {
    onDelete: "CASCADE",
  })
  show: Show;

  constructor(show: Show, color: string) {
    this.color = color;
    this.show = show;
  }
}
