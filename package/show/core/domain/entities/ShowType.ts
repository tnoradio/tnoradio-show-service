import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class ShowType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    //nullable: false,
    length: 50,
  })
  showType: string;

  constructor(showType) {
    this.showType = showType;
  }
}
