import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";

import { Show } from "./Show";

@Entity()
export class ShowImage {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 100,
    //nullable: true
    default: "showBanner",
  })
  name: string;

  @Column({
    type: "varchar",
    length: 100,
    nullable: true,
  })
  showSlug: string;

  @Column({
    type: "varchar",
    length: 100,
    nullable: true,
  })
  url: string;

  @Column({
    type: "longblob",
    nullable: true,
  })
  file: Buffer;

  @ManyToOne((type) => Show, (show) => show.images, {
    onDelete: "CASCADE",
  })
  show: Show;

  constructor(showId, name, showSlug, file, url) {
    this.name = name;
    this.showSlug = showSlug;
    this.show = showId;
    this.file = file;
    this.url = url;
  }
}
