import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
} from "typeorm";
import { ShowEpisode } from "./ShowEpisode";

@Entity()
export class ShowEpisodeTag {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 100,
    default: "Tag Name",
  })
  name: string;

  @ManyToMany(() => ShowEpisode, (episode) => episode.tags)
  @JoinTable({ name: "show_episode_tag_nan" })
  episodes: ShowEpisode[];
}
