import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  ManyToMany,
  JoinTable,
} from "typeorm";
import { Show } from "./Show";

@Entity()
export class ShowAdvertiser {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 100,
    default: "Advertiser Name",
  })
  name: string;

  @Column({
    type: "varchar",
    length: 100,
    nullable: true,
  })
  website: string;

  @Column({
    type: "longblob",
  })
  image: Buffer;

  @ManyToMany(() => Show, (show) => show.advertisers)
  @JoinTable({ name: "show_advertiser_nan" })
  shows: Show[];
}
