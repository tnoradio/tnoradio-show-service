import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  ManyToMany,
  JoinTable,
} from "typeorm";
import { ShowSeason } from "./ShowSeason";
import { ShowEpisodeTag } from "./ShowEpisodeTag";

@Entity()
export class ShowEpisode {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 100,
    default: "Episode Title",
  })
  title: string;

  @Column({
    type: "varchar",
    length: 500,
    default: "Episode Description",
  })
  description: string;

  @Column({
    type: "varchar",
    length: 100,
  })
  url: string;

  @Column({
    type: "varchar",
    length: 100,
  })
  miniature: string;

  @Column()
  episode_number: number;

  @ManyToOne((type) => ShowSeason, (season) => season.episodes, {
    onDelete: "CASCADE",
  })
  season: ShowSeason;

  @ManyToMany(() => ShowEpisodeTag, (tag) => tag.episodes)
  @JoinTable({ name: "show_episode_tag_nan" })
  tags: ShowEpisodeTag[];

  constructor(
    seasonId: ShowSeason,
    title: string,
    url: string,
    episode_number: number,
    description: string,
    miniature: string,
    tags: ShowEpisodeTag[],
  ) {
    this.season = seasonId;
    this.title = title;
    this.url = url;
    this.miniature = miniature;
    this.episode_number = episode_number;
    this.description = description;
    this.tags = tags;
  }
}
