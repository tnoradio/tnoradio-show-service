import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class ShowPGClassification {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    //nullable: false,
    length: 50,
  })
  showPGClassification: string;

  constructor(showpgclassification) {
    this.showPGClassification = showpgclassification;
  }
}
