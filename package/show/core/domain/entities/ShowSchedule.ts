import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Timestamp,
  ManyToOne,
  JoinColumn,
} from "typeorm";

import { Show } from "./Show";

export enum WeekDay {
  SUNDAY = "domingo",
  MONDAY = "lunes",
  TUESDAY = "martes",
  WEDNESDAY = "miercoles",
  THURSDAY = "jueves",
  FRIDAY = "viernes",
  SATURDAY = "sabado",
}

@Entity()
export class ShowSchedule {
  @PrimaryGeneratedColumn()
  id: number;

  /*@Column({
        type: 'varchar'
    })    
    fk_user_id: string;*/

  @Column({
    type: "varchar",
    //nullable: false,
    //enum: WeekDay,
    //default: WeekDay.MONDAY
  })
  weekDay: string;

  @Column({
    type: "time",
  })
  startTime: Timestamp;

  @Column({
    type: "time",
  })
  endTime: Timestamp;

  @ManyToOne(() => Show, (show) => show.showSchedules, { onDelete: "CASCADE" })
  showId: Number;

  constructor(showId, weekDay, startTime, endTime) {
    this.weekDay = weekDay;
    this.startTime = startTime;
    this.endTime = endTime;
    this.showId = showId;
  }
}
