import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinTable,
  ManyToMany,
} from "typeorm";
import { ShowGenre } from "./ShowGenre";
import { Show } from "./Show";

@Entity()
export class ShowSubGenre {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 50,
    unique: true,
    nullable: false,
  })
  subgenre: string;

  @ManyToMany(() => ShowGenre, (genre) => genre.subgenres)
  @JoinTable({ name: "show_genre_subgenre" })
  genres: ShowGenre[];

  @ManyToMany(() => Show, (show) => show.subgenres)
  @JoinTable({ name: "show_subgenre_nan" })
  shows: Show[];
}
