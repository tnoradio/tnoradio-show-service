import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from "typeorm";

import { ShowSchedule } from "./ShowSchedule";
import { ShowHost } from "./ShowHost";
import { ShowSocialMedia } from "./ShowSocialMedia";
import { ShowImage } from "./showImage";
import { ShowColor } from "./ShowColor";
import { ShowProducer } from "./ShowProducer";
import { ShowAdvertiser } from "./ShowAdvertiser";
import { ShowVideo } from "./ShowVideo";
import { ShowSeason } from "./ShowSeason";
import { ShowGenre } from "./ShowGenre";
import { ShowSubGenre } from "./ShowSubGenre";

@Entity()
export class Show {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 100,
    default: "Show Name",
  })
  name: string;

  @Column({
    type: "varchar",
    length: 50,
    default: "Show Genre",
  })
  genre: string;

  @Column({
    type: "varchar",
    length: 500,
    default: "Show Synopsis",
  })
  synopsis: string;

  @Column({
    type: "varchar",
    length: 50,
    default: "Show Type",
  })
  type: string;

  @Column({
    type: "varchar",
    length: 50,
    default: "Show PG",
  })
  pgClassification: string;

  @Column({
    type: "date",
    nullable: true,
  })
  initDate: Date;

  @Column({
    type: "varchar",
    length: 50,
    default: "info@tnoradio.com",
  })
  email: string;

  @Column({
    type: "varchar",
    length: 100,
    nullable: true,
  })
  updatedBy: string;

  @Column({
    type: "boolean",
    default: false,
  })
  isDeleted: Boolean;

  @Column({
    type: "boolean",
    default: false,
  })
  isEnabled: Boolean;

  @Column({
    type: "boolean",
    default: false,
  })
  isActive: Boolean;

  @Column({
    type: "boolean",
    default: false,
  })
  isPodcast: Boolean;

  @Column({
    type: "varchar",
    length: 50,
    nullable: true,
  })
  showSlug: string;

  @Column({
    type: "simple-array",
    nullable: true,
  })
  age_range: string[];

  @Column({
    type: "simple-array",
    nullable: true,
  })
  target: string[];

  @Column({
    type: "numeric",
    nullable: true,
  })
  likes: number;

  @Column({
    type: "numeric",
    nullable: true,
  })
  dislikes: number;

  @Column({
    type: "numeric",
    nullable: true,
  })
  views: number;

  @Column({
    type: "numeric",
    nullable: true,
  })
  rating: number;

  @Column({
    type: "simple-array",
    nullable: true,
  })
  target_gender: string[];

  @OneToMany((type) => ShowSchedule, (showSchedule) => showSchedule.showId, {
    cascade: true,
  })
  showSchedules: ShowSchedule[];

  @OneToMany((type) => ShowHost, (host) => host.showId, { cascade: true })
  hosts: ShowHost[];

  @OneToMany((type) => ShowProducer, (producer) => producer.show, {
    cascade: true,
  })
  producers: ShowProducer[];

  @OneToMany(() => ShowSocialMedia, (social) => social.show, {
    cascade: true,
  })
  socials: ShowSocialMedia[];

  @OneToMany(() => ShowImage, (image) => image.show, { cascade: true })
  images: ShowImage[];

  @OneToMany(() => ShowColor, (color) => color.show, { cascade: true })
  colors: ShowColor[];

  @ManyToMany(() => ShowSubGenre, (subgenre) => subgenre.shows)
  @JoinTable({ name: "show_subgenre_nan" })
  subgenres: ShowSubGenre[];

  @ManyToMany(() => ShowGenre, (genre) => genre.shows)
  @JoinTable({ name: "show_genre_nan" })
  genres: ShowGenre[];

  @ManyToMany(() => ShowAdvertiser, (advertiser) => advertiser.shows)
  @JoinTable({ name: "show_advertiser_nan" })
  advertisers: ShowAdvertiser[];

  @OneToMany((type) => ShowSeason, (season) => season.show, { cascade: true })
  seasons: ShowSeason[];

  @OneToMany((type) => ShowVideo, (video) => video.show, { cascade: true })
  videos: ShowVideo[];
}
