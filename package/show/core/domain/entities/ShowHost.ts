import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";

import { Show } from "./Show";

@Entity()
export class ShowHost {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    //nullable: false,
    length: 100,
    default: "Show User",
  })
  userId: string;

  @ManyToOne((type) => Show, (show) => show.hosts, {
    onDelete: "CASCADE",
  })
  showId: Show;

  constructor(showId, userId) {
    this.userId = userId;
    this.showId = showId;
  }
}
