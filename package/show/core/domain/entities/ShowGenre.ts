import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
  ManyToOne,
} from "typeorm";
import { ShowSubGenre } from "./ShowSubGenre";
import { Show } from "./Show";

@Entity()
export class ShowGenre {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    length: 50,
    unique: true,
    nullable: false,
  })
  genre: string;

  @ManyToMany(() => ShowSubGenre, (subgenre) => subgenre.genres)
  @JoinTable({ name: "show_genre_subgenre" })
  subgenres: ShowSubGenre[];

  @ManyToMany(() => Show, (show) => show.genres)
  @JoinTable({ name: "show_genre_nan" })
  shows: Show[];
}
