import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";

import { Show } from "./Show";

@Entity()
export class ShowProducer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: "varchar",
    //nullable: false,
    length: 100,
    default: "Show Producer",
  })
  userId: string;

  @ManyToOne((type) => Show, (show) => show.producers, {
    onDelete: "CASCADE",
  })
  show: Show;

  constructor(showId, userId) {
    this.userId = userId;
    this.show = showId;
  }
}
