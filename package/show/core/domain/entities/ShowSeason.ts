import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from "typeorm";

import { Show } from "./Show";
import { ShowEpisode } from "./ShowEpisode";

@Entity()
export class ShowSeason {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  season_number: number;

  @ManyToOne((type) => Show, (show) => show.seasons, {
    onDelete: "CASCADE",
  })
  show: Show;

  @OneToMany((type) => ShowEpisode, (episode) => episode.season, {
    cascade: true,
  })
  episodes: ShowEpisode[];

  constructor(showId: Show, season_number: number, episodes: ShowEpisode[]) {
    this.season_number = season_number;
    this.show = showId;
    this.episodes = episodes;
  }
}
