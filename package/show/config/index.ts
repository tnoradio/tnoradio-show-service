import dotenv from "dotenv";

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || "development";

const envFound = dotenv.config();
if (envFound.error) {
  // This error should crash whole process

  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

export default {
  /**
   * Your favorite port
   */
  port: parseInt(process.env.PORT, 10) || 4000,

  /**
   * Mysql
   */
  dbPort: parseInt(process.env.DB_PORT),
  dbName: process.env.DB_NAME,
  dbUsername: process.env.DB_USERNAME,
  dbPassword: process.env.DB_PASSWORD,
  dbHost: process.env.DB_HOST,

  /**
   * Used by winston logger
   */
  logs: {
    level: process.env.LOG_LEVEL || "silly",
  },
  /**
   * JWT configuration
   */
  jwt_expired_token: process.env.EXPIRED_TOKEN,
  jwt_seed: process.env.SEED_AUTENTICATION,

  /**
   * Kafka
   */
  kafka_host: process.env.KAFKA_HOST,

  /**
   * Google Drive
   */
  parent_folder: process.env.PARENT_FOLDER_URL,
};
