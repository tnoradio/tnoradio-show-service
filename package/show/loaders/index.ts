import expressLoader from "./express";
import logger from "./logger";
import express from "express";
import config from "../config";
//import kafka from "./kafka";
//import consul from "./consul";

export const app = express();

//Adds Express Static Middleware
app.use(express.static("public"));

//Serves all the request which includes /images in the url from Images folder
//app.use('/images', express.static(__dirname + 'public/images/'));

//kafka.consumer.on("message", function (message) {
//  console.log(message);
//});

export default async () => {
  /**
   * Port loader
   */
  await app.listen(config.port || 4000, () => {
    try {
      /**
       * Consul Register
       */

      // consul.register;
      logger.info(`
        ################################################
        🛡️  Server listening on port: ${config.port} 🛡️ 
        ################################################
      `);
    } catch (err) {
      logger.error(err);
      process.exit(1);
    }
  });

  /**
   * Laods express essentials
   */
  await expressLoader({ app });
  logger.log("info", "Express Loader has initalized successfully! ✅");
};
