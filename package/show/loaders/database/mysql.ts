import { createConnection, Connection } from "typeorm";
import logger from "../logger";
import global from "../../config";

const connection = createConnection()
  .then((connection) => {
    logger.info("✌️ DB MySQL loaded and connected !");
    logger.warn(
      "Database host ----->> " + process.env.TNO_DB_HOST + " <<-----",
    );
  })
  .catch((error) => {
    logger.error(error);
  });

if (connection === undefined) {
  throw new Error("Error connecting to database");
} // In case the connection failed, the app stops.

//connection.synchronize(); // this updates the database schema to match the models' definitions

//this.connection = connection; // Store the connection object in the class instance.
