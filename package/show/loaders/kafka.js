import kafka from "kafka-node";
import config from "../config";

const client = new kafka.KafkaClient({ kafkaHost: config.kafka_host });

var consumer = new kafka.Consumer(client, [{ topic: "users" }]);

var producer = new kafka.Producer(client);

client.on("error", (e) => {
  console.log("Error arrancando client");
  console.log(e);
});
client.on("ready", () => {
  console.log("Kafka client ready");
});

consumer.on("error", (e) => {
  console.log("Error arrancando consumer");
  console.log(e);
});

consumer.on("ready", () => {
  console.log("Kafka consumer is ready");
});

producer.on("error", (e) => {
  console.log("Error arrancando producer");
  console.log(e);
});

producer.on("ready", () => {
  console.log("Kafka producer is ready");
});

export default {
  consumer: consumer,
  producer: producer,
};
