import jwtoken from "jsonwebtoken";
import config from "../config";
import { Show } from "../core/domain/entities/Show";

/**
 * Generate token json
 * @param user
 */
async function generateToken(user: Show) {
  try {
    //Generate Autentication token
    let token = jwtoken.sign(
      {
        userAcces: user,
      },
      config.jwt_seed,
      { expiresIn: config.jwt_expired_token },
    );

    //retun token
    return {
      ok: true,
      user: user,
      token,
    };
  } catch (err) {
    return err;
  }
}

const jwt = {
  generateToken,
};

export default jwt;
