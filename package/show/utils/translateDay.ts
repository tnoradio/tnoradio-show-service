export function translateDay(day: number) {
  switch (day) {
    case 0:
      return "domingo";
    case 1:
      return "lunes";
    case 2:
      return "martes";
    case 3:
      return "miercoles";
    case 4:
      return "jueves";
    case 5:
      return "viernes";
    case 6:
      return "sabado";
    default:
      return "lunes";
  }
}

export function getTime(time: String): number[] {
  var pieces = time.toString().split(":");
  var hour;
  var minute;
  var second;

  var resTime: number[] = [];

  if (pieces.length === 3) {
    hour = parseInt(pieces[0], 10);
    minute = parseInt(pieces[1], 10);
    second = parseInt(pieces[2], 10);
  }

  resTime.push(hour);
  resTime.push(parseInt(checkMinutesLength(minute)));

  return resTime;
}

function checkMinutesLength(minutes): string {
  if (minutes < 10) return "0" + minutes.toString();
  else return minutes.toString();
}
