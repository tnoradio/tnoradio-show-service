module.exports = {
  type: "mysql",
  host: process.env.TNO_DB_HOST,
  port: process.env.TNO_DB_PORT,
  username: process.env.TNO_DB_USER,
  password: process.env.TNO_DB_PASSWORD,
  database: process.env.TNO_DB_NAMEDATABASE,
  synchronize: true,
  logging: false,
  entities: ["package/show/core/domain/entities/**/*.ts"],
  migrations: ["package/show/core/domain/migrations/**/*.ts"],
  subscribers: ["package/show/core/domain/subscribers/**/*.ts"],
  cli: {
    entitiesDir: "package/show/core/domain/entities",
    migrationsDir: "package/show/core/domain/migrations",
    subscribersDir: "package/show/core/domain/subscribers",
  },
  extra: {
    connectTimeout: 10000, // 10 seconds
    acquireTimeout: 30000, // 30 seconds
  },
  retryAttempts: 5, // Number of retry attempts
  retryDelay: 3000, // Delay between retry attempts (in ms)
};
