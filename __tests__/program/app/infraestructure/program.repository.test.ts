import _mock from "../../../../__mocks__/show.mock";
import { ShowMysqlRepository } from "../../../../package/show/app/infrastructure/show.mysql.repository";
import { Show } from "../../../../package/show/app/domain/entities/Show";

describe("Repository", () => {
  let repositoryMysql = new ShowMysqlRepository();

  test("Create new Show", async () => {
    let repositorySpy = jest.spyOn(repositoryMysql, "createShow");

    let request = repositoryMysql.createShow(_mock.showMock);

    expect(repositorySpy).toHaveBeenCalled();
    expect(repositorySpy).toBeCalledWith(expect.any(Show));
  });

  test("Get all shows", async () => {
    let getAllSpy = jest.spyOn(repositoryMysql, "getAllShows");
    let response = await repositoryMysql.getAllShows();

    expect(getAllSpy).toHaveBeenCalled();
  });

  test("get by id Show", async () => {
    let byIdSpy = jest.spyOn(repositoryMysql, "getShowById");

    let response = await repositoryMysql.getShowById("0000");

    expect(byIdSpy).toHaveBeenCalled();
    expect(byIdSpy).toBeCalledWith(expect.any(String));
  });

  test("get by name Show", async () => {
    let byIdSpy = jest.spyOn(repositoryMysql, "getShowsByName");

    let response = await repositoryMysql.getShowsByName("prueba");

    expect(byIdSpy).toHaveBeenCalled();
    expect(byIdSpy).toBeCalledWith(expect.any(String));
  });

  test("get shows by fk user id", async () => {
    let byIdSpy = jest.spyOn(repositoryMysql, "getShowsAuthorizedByUserId");

    let response = await repositoryMysql.getShowsAuthorizedByUserId("1111");

    expect(byIdSpy).toHaveBeenCalled();
    expect(byIdSpy).toBeCalledWith(expect.any(String));
  });

  test("update", async () => {
    let byIdSpy = jest.spyOn(repositoryMysql, "updateShow");

    let response = await repositoryMysql.updateShow(_mock.showMock);

    expect(byIdSpy).toHaveBeenCalled();
    expect(byIdSpy).toBeCalledWith(expect.any(Show));
  });
});
