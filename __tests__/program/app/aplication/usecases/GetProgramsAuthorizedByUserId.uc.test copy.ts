import {
  ShowAuthorizedByUserId,
  Services,
} from "../../../../../package/show/app/application/usecases/ShowAuthorizedByUserId.uc";
import { ShowMysqlRepository } from "../../../../../package/show/app/infrastructure/show.mysql.repository";

describe("Use Case: Get show Authorized by Id", () => {
  let showRepository = new ShowMysqlRepository();
  let showAuthorized = new ShowAuthorizedByUserId({ showRepository });

  beforeAll(() => {
    showAuthorized.setIdUser(1111);
  });

  test("execute function", async () => {
    let executeSpy = jest.spyOn(showAuthorized, "execute");

    await showAuthorized.execute();

    expect(executeSpy).toHaveBeenCalled();
  });

  test("get and set id show", () => {
    let getIdSpy = jest.spyOn(showAuthorized, "getIdUser");
    expect(showAuthorized.getIdUser()).toEqual(1111);
    expect(getIdSpy).toHaveBeenCalled();
  });
});
