import {
  ShowSelect,
  Services,
} from "../../../../../package/show/app/application/usecases/ShowSelect.uc";
import _mock from "../../../../../__mocks__/show.mock";
import { ShowMysqlRepository } from "../../../../../package/show/app/infrastructure/show.mysql.repository";

describe("Get all shows : Use Cases", () => {
  //let iServices: Services;
  let showRepository = new ShowMysqlRepository();
  let useCase = new ShowSelect({ showRepository });

  test("called function execute", async () => {
    let executeSpy = jest.spyOn(useCase, "execute");
    await useCase.execute();
    expect(executeSpy).toHaveBeenCalledTimes(1);
  });
});
