import {
  ShowById,
  Services,
} from "../../../../../package/show/app/application/usecases/ShowById.uc";
import { ShowMysqlRepository } from "../../../../../package/show/app/infrastructure/show.mysql.repository";

describe("Use Case: Get show by Id", () => {
  let showRepository = new ShowMysqlRepository();
  let showById = new ShowById({ showRepository });

  beforeAll(() => {
    showById.setIdShow("0000");
  });

  test("execute function", async () => {
    let executeSpy = jest.spyOn(showById, "execute");

    await showById.execute();

    expect(executeSpy).toHaveBeenCalled();
  });

  test("get and set id show", () => {
    let getIdSpy = jest.spyOn(showById, "getIdShow");
    expect(showById.getIdShow()).toEqual("0000");
    expect(getIdSpy).toHaveBeenCalled();
  });
});
