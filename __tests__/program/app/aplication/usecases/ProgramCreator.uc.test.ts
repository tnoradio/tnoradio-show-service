import {
  ShowCreator,
  Services,
} from "../../../../../package/show/app/application/usecases/ShowCreator.uc";
import _mock from "../../../../../__mocks__/show.mock";
describe("Use Cases", () => {
  let iServices: Services;
  let useCase = new ShowCreator(iServices);

  beforeAll(() => {
    useCase.setShow(_mock.showMock);
  });

  test("Create show object", async () => {
    let objSpy = jest.spyOn(useCase, "setShow");
    useCase.setShow(_mock.showMock);
    expect(objSpy).toHaveBeenCalled();
  });

  test("Get show object", async () => {
    let show = useCase.getShow();
    expect(show).toEqual(_mock.showMock);
  });
});
