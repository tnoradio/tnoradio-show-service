import {
  ShowByName,
  Services,
} from "../../../../../package/show/app/application/usecases/ShowByName.uc";
import { ShowMysqlRepository } from "../../../../../package/show/app/infrastructure/show.mysql.repository";

describe("Use Case: Get show by Name", () => {
  let showRepository = new ShowMysqlRepository();
  let showByName = new ShowByName({ showRepository });

  beforeAll(() => {
    showByName.setNameShow("test");
  });

  test("execute function", async () => {
    let executeSpy = jest.spyOn(showByName, "execute");

    await showByName.execute();

    expect(executeSpy).toHaveBeenCalled();
  });

  test("get and set id show", () => {
    let getIdSpy = jest.spyOn(showByName, "getNameShow");
    expect(showByName.getNameShow()).toEqual("test");
    expect(getIdSpy).toHaveBeenCalled();
  });
});
