import {
  ShowUpdateById,
  Services,
} from "../../../../../package/show/app/application/usecases/ShowUpdate.uc";
import { ShowMysqlRepository } from "../../../../../package/show/app/infrastructure/show.mysql.repository";
import _mock from "../../../../../__mocks__/show.mock";

describe("Use Case: Update show by Id", () => {
  let showRepository = new ShowMysqlRepository();
  let show = new ShowUpdateById({ showRepository });

  beforeAll(() => {
    show.setShow(_mock.showMock);
  });

  test("execute function", async () => {
    let executeSpy = jest.spyOn(show, "execute");

    await show.execute();

    expect(executeSpy).toHaveBeenCalled();
  });

  test("get and set show", () => {
    let getIdSpy = jest.spyOn(show, "getShow");
    expect(show.getShow()).toEqual(_mock.showMock);
    expect(getIdSpy).toHaveBeenCalled();
  });
});
