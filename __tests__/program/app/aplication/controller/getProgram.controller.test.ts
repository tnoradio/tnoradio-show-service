import {
  SelectedShowController,
  UsesCases,
} from "../../../../../package/show/app/application/controller/getShow.controller";
import { ShowSelect } from "../../../../../package/show/app/application/usecases/ShowSelect.uc";
import { ShowMysqlRepository } from "../../../../../package/show/app/infrastructure/show.mysql.repository";
import _mock from "../../../../../__mocks__/httpCreate.post.mock";

describe.skip(" Controller; Get all shows", () => {
  let showRepository = new ShowMysqlRepository();
  let getShows = new ShowSelect({ showRepository });
  let showSelectController = new SelectedShowController({ getShows });

  test("handle function", async () => {
    let handleSpy = jest.spyOn(showSelectController, "handle");

    showSelectController.handle(_mock.mockRequest, _mock.mockResponse);

    expect(handleSpy).toHaveBeenCalled();
  });
});
