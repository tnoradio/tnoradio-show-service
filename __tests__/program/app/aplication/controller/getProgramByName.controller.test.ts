import {
  GetByNameShowController,
  UsesCases,
} from "../../../../../package/show/app/application/controller/getShowByName.controller";
import { ShowByName } from "../../../../../package/show/app/application/usecases/ShowByName.uc";
import { ShowMysqlRepository } from "../../../../../package/show/app/infrastructure/show.mysql.repository";
import _mock from "../../../../../__mocks__/httpCreate.post.mock";

describe.skip("Controller: get Show by Name", () => {
  let showRepository = new ShowMysqlRepository();
  let getShowByName = new ShowByName({ showRepository });
  let showNameController = new GetByNameShowController({ getShowByName });

  test.skip("handle function", async () => {
    let handleSpy = jest.spyOn(showNameController, "handle");

    //MISMO ERROR CON EL SEND
    showNameController.handle(_mock.mockRequest, _mock.mockResponse);

    expect(handleSpy).toHaveBeenCalled();
  });
});
