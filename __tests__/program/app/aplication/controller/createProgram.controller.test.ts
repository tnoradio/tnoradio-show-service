import {
  CreateShowController,
  UsesCases,
} from "../../../../../package/show/app/application/controller/createShow.controller";
import _mocks from "../../../../../__mocks__/httpCreate.post.mock";
import { ShowCreator } from "../../../../../package/show/app/application/usecases/ShowCreator.uc";
import { ShowMysqlRepository } from "../../../../../package/show/app/infrastructure/show.mysql.repository";

describe.skip("Create show : Controller", () => {
  const showRepository = new ShowMysqlRepository();
  const createShow = new ShowCreator({ showRepository });
  const showCreatorController = new CreateShowController({ createShow });

  test("Handle function create", async () => {
    let handleSpy = jest.spyOn(showCreatorController, "handle");
    await showCreatorController.handle(_mocks.mockRequest, _mocks.mockResponse);
    //Error send function
    expect(handleSpy).toHaveBeenCalled();
  });
});
