import {
  GetByIdShowController,
  UsesCases,
} from "../../../../../package/show/app/application/controller/getShowById.controller";
import { ShowById } from "../../../../../package/show/app/application/usecases/ShowById.uc";
import { ShowMysqlRepository } from "../../../../../package/show/app/infrastructure/show.mysql.repository";
import _mock from "../../../../../__mocks__/httpCreate.post.mock";

describe.skip("Controller: get Show by Id", () => {
  let showRepository = new ShowMysqlRepository();
  let getShowById = new ShowById({ showRepository });
  let showIdController = new GetByIdShowController({ getShowById });

  test.skip("handle function", async () => {
    let handleSpy = jest.spyOn(showIdController, "handle");

    //MISMO ERROR CON EL SEND
    showIdController.handle(_mock.mockRequest, _mock.mockResponse);

    expect(handleSpy).toHaveBeenCalled();
  });
});
