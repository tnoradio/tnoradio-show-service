import { Show } from "../package/show/app/domain/entities/Show";

const showMock = new Show(
  "tnoradioshowa@gmail.com",
  "showaxs",
  "Diego",
  "Salud y bienestar",
  "Showa sobre la salud",
  "1111",
);

export default {
  showMock,
};
