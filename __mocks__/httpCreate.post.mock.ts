const mockRequest: any = {
  body: {
    email: "elbergjessica@gmail.com",
    facebook: "hola",
    producer: "Jessica",
  },
};

const mockResponse: any = {
  json: jest.fn(),
  status: jest.fn(),
};

export default { mockRequest, mockResponse };
