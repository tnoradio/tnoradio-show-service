Dependencies

Mocha: Creates tests.
Sinon: Creates mockups.
SuperTest: Test Runner.
Express: Server for javascript.
MySql: Data Base.
BodyParser: Parsin objects.
DotEnv: Set Environment variables.
